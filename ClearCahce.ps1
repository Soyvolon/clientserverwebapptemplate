Write-Host "Repository Cleaner" -ForegroundColor Magenta;
Write-Host "";
Write-Host "========= README =========" -ForegroundColor Red;
Write-Host "Please ensure all work environements are closed before running this script (vscode, visual studio, etc.)." -ForegroundColor Red;
Write-Host "This script will clear the .vs folder and the obj and bin folders for each project. If you have any data in those folders you need to keep, please back them up now." -ForegroundColor Red;
Write-Host "========= README =========" -ForegroundColor Red;
Write-Host "";
Write-Host "-----" -ForegroundColor Magenta;

$continuePrompt = Read-Host -Prompt "Continue? [(y)es/(c)lean/(n)o]";

if ($continuePrompt -ne 'y' -and $continuePrompt -ne 'c') {
    Write-Host "Aborting..." -ForegroundColor Red;
    Return;
}

$slnFile = [System.IO.Path]::GetFullPath((Get-ChildItem -Path .\ -Filter *.sln -Recurse -File)[0].Name);
$slnFolder = [System.IO.Path]::GetDirectoryName($slnFile);

if ($continuePrompt -eq 'c') {
    Write-Host "-----" -ForegroundColor Magenta;
    Write-Host "Running msbuild clean command." -ForegroundColor Magenta;
    Write-Host "-----" -ForegroundColor Magenta;

    $msbuild = &"${env:ProgramFiles(x86)}\Microsoft Visual Studio\Installer\vswhere.exe" -latest -prerelease -products * -requires Microsoft.Component.MSBuild -find MSBuild\**\Bin\MSBuild.exe;

    &"$msbuild" $slnFile "/target:Clean";
}

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Clearing cached data." -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;

$vsFolder = (Get-ChildItem -Path "$slnFolder" -Depth 1 -Filter *.vs -Directory);

if ($vsFolder.Exists) {
    Remove-Item ($vsFolder.FullName) -Recurse -Force;
}

$paths = @();
try {
    [Regex]$csprojRegex = '^Project.*"(.*\.csproj)".*$';
    $slnData = [System.IO.File]::OpenText($slnFile);

    while (!$slnData.EndOfStream) {
        $text = $slnData.ReadLine();
        $groups = $csprojRegex.Matches($text);
        for ($i = 0; $i -lt $groups.Count; $i++) {
            <# Action that will repeat until the condition is met #>
            $proj = $groups[$i].Groups[1].Value;
            $paths += [System.IO.Path]::GetDirectoryName($proj);
        }
    }
}
catch {
    Write-Host "An error occured reading SLN data." -ForegroundColor Red;
}
finally {
    $slnData.Close();
}

for ($i = 0; $i -lt $paths.Count; $i++) {
    $folder = [System.IO.Path]::Combine($slnfolder, $paths[$i]);
    
    $bin = (Get-ChildItem -Path "$folder" -Depth 1 -Filter bin -Directory);
    if ($bin.Exists) {
        Remove-Item ($bin.FullName) -Recurse -Force;
    }
    
    $obj = (Get-ChildItem -Path "$folder" -Depth 1 -Filter obj -Directory);
    if ($obj.Exists) {
        Remove-Item ($obj.FullName) -Recurse -Force;
    }
}

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Cleanup complete." -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;
