namespace ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

public partial interface IScopedDataBus
{
    /// <summary>
    /// The theme of the website.
    /// </summary>
    public string Theme { get; set; }
}
