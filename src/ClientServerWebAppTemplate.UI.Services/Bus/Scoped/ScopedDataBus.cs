﻿using System.Security.Claims;

namespace ClientServerWebAppTemplate.UI.Services.Bus.Scoped;
/// <summary>
/// Template Description
/// </summary>
public partial class ScopedDataBus : IScopedDataBus
{
    public ClaimsPrincipal? CurrentUserClaims { get; set; }
}
