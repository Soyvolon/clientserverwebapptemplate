﻿using ClientServerWebAppTemplate.Structures.Events;

namespace ClientServerWebAppTemplate.UI.Services.Bus.Scoped;
public partial interface IScopedDataBus
{
    public delegate Task DisplayMenuEventHandler(object sender, DisplayMenuEventArgs args);
    public event DisplayMenuEventHandler DisplayMenu;
    public Task DisplayMenuAsync(object sender, DisplayMenuEventArgs args);

    public delegate Task CloseMenuEventHandler(object sender, string id);
    public event CloseMenuEventHandler CloseMenu;
    public Task CloseMenuAsync(object sender, string id);

    public delegate Task MenuClosedEventHandler(object sender, DisplayMenuEventArgs args);
    public event MenuClosedEventHandler MenuClosed;
    public Task MenuClosedAsync(object sender, DisplayMenuEventArgs args);

    public delegate Task ReloadMenuEventHandler(object sender);
    public event ReloadMenuEventHandler ReloadMenu;
    public Task RequestMenuReload(object sender);
}
