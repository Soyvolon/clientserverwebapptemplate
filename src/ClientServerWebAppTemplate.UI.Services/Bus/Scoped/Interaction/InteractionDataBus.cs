﻿using ClientServerWebAppTemplate.Structures.Events;

namespace ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

public partial class ScopedDataBus
{
    public event IScopedDataBus.PageClickedEventHandler? PageClicked;
    public Task SendPageClickEventAsync(object sender, PageClickedEventArgs args)
    {
        try
        {
            if (PageClicked is not null)
                _ = Task.Run(async () => await PageClicked.Invoke(sender, args));
        }
        catch { }

        return Task.CompletedTask;
    }

    public event IScopedDataBus.KeyPressedEventHandler? KeyPressed;
    public Task SendKeyPressEventAsync(object sender, OnPressEventArgs args)
    {
        try
        {
            if (KeyPressed is not null)
                _ = Task.Run(async () => await KeyPressed.Invoke(sender, args));
        }
        catch { }

        return Task.CompletedTask;
    }
}
