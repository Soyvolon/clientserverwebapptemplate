using ClientServerWebAppTemplate.Structures.Events;

namespace ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

public partial interface IScopedDataBus
{
    public delegate Task PageClickedEventHandler(object sender, PageClickedEventArgs args);
    public event PageClickedEventHandler PageClicked;
    public Task SendPageClickEventAsync(object sender, PageClickedEventArgs args);

    public delegate Task KeyPressedEventHandler(object sender, OnPressEventArgs args);
    public event KeyPressedEventHandler KeyPressed;
    public Task SendKeyPressEventAsync(object sender, OnPressEventArgs args);
}

