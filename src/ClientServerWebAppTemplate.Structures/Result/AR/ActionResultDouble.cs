﻿using System.Diagnostics.CodeAnalysis;

namespace ClientServerWebAppTemplate.Structures.Result;

/// <summary>
/// An <see cref="ActionResult"/> with an alternate result.
/// </summary>
/// <typeparam name="Res">The result type.</typeparam>
/// <typeparam name="Alt">The alternate result.</typeparam>
/// <param name="success">If true, the result is a success.</param>
/// <param name="errors">If <paramref name="success"/> is false, this value will have a list of error messages.</param>
/// <param name="result">If <paramref name="success"/> is true, this value will have the result.</param>
/// <param name="altResult">If <paramref name="success"/> is false, this value will have an alternative result.</param>
public class ActionResult<Res, Alt>(
        bool success,
        List<string>? errors = null,
        Res? result = default,
        Alt? altResult = default
    ) : ActionResult<Res>(success, errors, result)
{
    /// <summary>
    /// The alternate result value.
    /// </summary>
    public Alt? AlternateResult { get; init; } = altResult;

    /// <summary>
    /// Gets a result or the alternate result.
    /// </summary>
    /// <param name="errors">The list of errors if false.</param>
    /// <param name="result">The result if true.</param>
    /// <param name="alt">The alternate result if false.</param>
    /// <returns>A boolean value indicating the success of the action.</returns>
    public bool GetResult([NotNullWhen(false)] out List<string>? errors,
        [NotNullWhen(true)] out Res? result,
        [NotNullWhen(false)] out Alt? alt)
    {
        if (Success)
        {
            errors = null;
            result = Result ?? default;
            alt = default;
        }
        else
        {
            errors = Errors ?? [];
            result = default;
            alt = AlternateResult ?? default;
        }

        return Success;
    }
}