﻿namespace ClientServerWebAppTemplate.Structures.Result;


/// <summary>
/// A version of <see cref="ActionResult{T}"/> for JSON sterilization.
/// </summary>
/// <remarks>
/// Create a new instance.
/// </remarks>
/// <param name="success">Sets <see cref="ActionResult.Success"/>.</param>
/// <param name="errors">Sets <see cref="ActionResult.Errors"/>.</param>
/// <param name="result">Sets <see cref="Result"/>.</param>
public class ActionResultJson(bool success, List<string>? errors = null, object? result = default) : ActionResult(success, errors)
{
    /// <summary>
    /// The result when <see cref="ActionResult.Success"/> is true.
    /// </summary>
    public object? Result { get; init; } = result;
}