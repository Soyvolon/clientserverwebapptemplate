using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json.Serialization.Metadata;

namespace ClientServerWebAppTemplate.Structures.Result;

/// <summary>
/// Json related settings for <see cref="ActionResult"/>.
/// </summary>
public static class ActionResultJsonSettings
{
    /// <summary>
    /// Gets a sterilization options object for polymorphism enabled result objects.
    /// </summary>
    /// <typeparam name="T">The type that needs polymorphism support.</typeparam>
    /// <returns>A <see cref="JsonSerializerOptions"/> that supports polymorphism for
    /// <see cref="ActionResult{T}"/>.</returns>
    public static JsonSerializerOptions GetActionResultOptionsFor<T>()
        where T : ActionResult
        => GetActionResultOptionsFor(typeof(T));

    /// <summary>
    /// Gets a sterilization options object for polymorphism enabled result objects.
    /// </summary>
    /// <param name="type">The type that needs polymorphism support.</param>
    /// <returns>A <see cref="JsonSerializerOptions"/> that supports polymorphism for
    /// <see cref="ActionResult{T}"/>.</returns>
    public static JsonSerializerOptions GetActionResultOptionsFor(Type type)
    {
        var resolver = new DefaultJsonTypeInfoResolver
        {
            Modifiers =
            {
                // Add an Action<JsonTypeInfo> modifier that sets up the polymorphism options for BaseType
                // In this case, we take the type and type name for the generic type.
                typeInfo =>
                {
                    if (!type.IsAssignableTo(typeof(ActionResult)))
                        return;

                    typeInfo.PolymorphismOptions = new()
                    {
                        DerivedTypes =
                        {
                            new JsonDerivedType(type, type.Name),
                        }
                    };

                    typeInfo.PolymorphismOptions.UnknownDerivedTypeHandling
                        = JsonUnknownDerivedTypeHandling.FallBackToNearestAncestor;
                },
            }
        };

        var options = new JsonSerializerOptions
        {
            TypeInfoResolver = resolver,
        };

        return options;
    }
}
