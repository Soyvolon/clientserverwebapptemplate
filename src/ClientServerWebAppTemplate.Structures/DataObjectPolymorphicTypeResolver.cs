using System.Text.Json;
using System.Text.Json.Serialization;
using System.Text.Json.Serialization.Metadata;

namespace ClientServerWebAppTemplate.Structures;

/// <summary>
/// A type resolve for automatically building out type information for 
/// <see cref="DataObject"/>s and all their child elements.
/// </summary>
public class DataObjectPolymorphicTypeResolver : DefaultJsonTypeInfoResolver
{
    ///<inheritdoc/>
    public override JsonTypeInfo GetTypeInfo(Type type, JsonSerializerOptions options)
    {
        var typeInfo = base.GetTypeInfo(type, options);

        Type dataObjectType = typeof(DataObject);

        if (!typeInfo.Type.IsAssignableTo(dataObjectType))
            return typeInfo;

        var polyOpts = new JsonPolymorphismOptions()
        {
            TypeDiscriminatorPropertyName = "$do-type",
            IgnoreUnrecognizedTypeDiscriminators = true,
            UnknownDerivedTypeHandling = JsonUnknownDerivedTypeHandling.FailSerialization
        };

        var assemblies = AppDomain.CurrentDomain.GetAssemblies();
        var derivedTypes = assemblies.SelectMany(e =>
        {
            try
            {
                var types = e.GetTypes()
                    .Where(e => e.IsAssignableTo(typeInfo.Type))
                    .Where(e => !e.IsAbstract);

                return types;
            }
            catch
            {
                return [];
            }
        });

        foreach (var derived in derivedTypes)
        {
            var derivedOpts = new JsonDerivedType(derived, derived.AssemblyQualifiedName ?? derived.Name);
            polyOpts.DerivedTypes.Add(derivedOpts);
        }

        typeInfo.PolymorphismOptions = polyOpts;

        return typeInfo;
    }
}
