﻿namespace ClientServerWebAppTemplate.Structures.Mail;

/// <summary>
/// The mail configuration.
/// </summary>
public class MailConfiguration
{
    /// <summary>
    /// The host
    /// </summary>
    public string Host { get; set; } = "";
    /// <summary>
    /// The port
    /// </summary>
    public int Port { get; set; }
    /// <summary>
    /// If true, a login is needed.
    /// </summary>
    public bool RequireLogin { get; set; }
    /// <summary>
    /// The username to login with.
    /// </summary>
    public string User { get; set; } = "";
    /// <summary>
    /// The password to login with.
    /// </summary>
    public string Password { get; set; } = "";
    /// <summary>
    /// The email to send from.
    /// </summary>
    public string Email { get; set; } = "";
}
