namespace ClientServerWebAppTemplate.Structures.Enums;

// https://gitlab.com/Soyvolon/DataCore/-/blob/8cf5568538c97c11e1639163c7c61f122647e92a/ProjectDataCore.Data/Structures/Model/Alert/AlertType.cs
public enum AlertType
{
    Info,
    Success,
    Warn,
    Error
}
