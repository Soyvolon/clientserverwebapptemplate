namespace ClientServerWebAppTemplate.Structures.Http.Services
{
    [AttributeUsage(AttributeTargets.Method)]
    public class APIReturnTypeAttribute(Type type) : Attribute
    {
        public Type Type { get; set; } = type;
    }
}
