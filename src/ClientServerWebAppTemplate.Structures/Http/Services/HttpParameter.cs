namespace ClientServerWebAppTemplate.Structures.Http.Services
{
    public struct HttpParameter(string name, string type, object? value)
    {
        public string Name { get; set; } = name;
        public string Type { get; set; } = type;
        public object? Value { get; set; } = value;
    }
}
