using ClientServerWebAppTemplate.Structures.Result;

using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.Extensions.DependencyInjection;

using System.Net.Http.Json;
using System.Text.Json;

namespace ClientServerWebAppTemplate.Structures.Http.Services
{
    public class HTTPCallService
    {
        public static readonly JsonSerializerOptions JsonOptions = new()
        {
            PropertyNameCaseInsensitive = true,
        };

        private readonly IHttpClientFactory _httpClientFactory;
        private readonly AuthenticationStateProvider _authStateProvider;

        public HTTPCallService(IServiceProvider provider)
        {
            _httpClientFactory = provider.GetRequiredService<IHttpClientFactory>();
            _authStateProvider = provider.GetRequiredService<AuthenticationStateProvider>();
        }

        /// <summary>
        /// Executes a REST request to the API.
        /// </summary>
        /// <param name="method">The method to use.</param>
        /// <param name="url">The URL to send a request to. This should not include the base URL.</param>
        /// <param name="bearerToken">If true, an auth token is present in the current auth identity.</param>
        /// <param name="parameters">HTTP parameters for this request.</param>
        /// <returns>An <see cref="ActionResult{T}"/> containing the JSON result of the request.</returns>
        public async Task<ActionResult<string>> ExecuteRestRequestAsync(string method, string url, bool bearerToken, params HttpParameter[]? parameters)
        {
            HttpRequestMessage message = new(new(method), url);

            if (bearerToken)
            {
                var state = await _authStateProvider.GetAuthenticationStateAsync();
                var token = state.User.FindFirst("access_token");

                if (token is not null && !string.IsNullOrWhiteSpace(token.Value))
                    message.Headers.Add("Authorization", $"Bearer {token.Value}");
            }

            foreach (var param in parameters ?? [])
                message.AddParameter(param.Name, param.Type, param.Value);

            using var client = _httpClientFactory.CreateClient(nameof(HTTPCallService));

            var res = await client.SendAsync(message);

            try
            {
                var content = await res.Content.ReadAsStringAsync();

                if (res.IsSuccessStatusCode)
                {
                    return new(true, null, content);
                }

                return new(false, [res.StatusCode.ToString(), content], null);
            }
            catch (Exception ex)
            {
                const string errMsg = "An unexpected error occurred";
                return new(false, [errMsg, ex.Message], null);
            }
        }
    }

    internal static class HttpRequestMessageExtensions
    {
        public static HttpRequestMessage AddParameter(this HttpRequestMessage request, string name, string type, object? value)
            => type.ToLower() switch
            {
                "body" => AddBodyParameter(request, value),
                "query" => AddQueryParameter(request, name, value),
                _ => request
            };

        private static HttpRequestMessage AddBodyParameter(HttpRequestMessage request, object? value)
        {
            request.Content = JsonContent.Create(value);

            return request;
        }

        private static HttpRequestMessage AddQueryParameter(HttpRequestMessage request, string name, object? value)
        {
            var query = request.RequestUri?.ToString() ?? "";

            if (query.Contains('?'))
                query += "&";
            else query += "?";

            query += $"{Uri.EscapeDataString(name)}={Uri.EscapeDataString(value?.ToString() ?? "")}";

            if (Uri.TryCreate(query, UriKind.Relative, out var uri))
            {
                request.RequestUri = uri;
            }

            return request;
        }

        private static HttpRequestMessage AddFormParameter(HttpRequestMessage request, string name, object? value)
        {
            if (request.Content is not MultipartFormDataContent)
                request.Content = new MultipartFormDataContent();

            MultipartFormDataContent content = (request.Content as MultipartFormDataContent)!;

            // TODO: Handle this much better. Content may not be string, it may be a file, etc.
            content.Add(new StringContent(value as string ?? ""), name);

            return request;
        }
    }
}
