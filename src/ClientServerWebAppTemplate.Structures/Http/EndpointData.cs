namespace ClientServerWebAppTemplate.Structures.Http
{
    /// <summary>
    /// Holds data about API endpoints.
    /// </summary>
    public class EndpointData
    {
        /// <summary>
        /// The HTTP Method for this endpoint.
        /// </summary>
        public string? Method { get; set; }
        /// <summary>
        /// The relative route for this endpoint.
        /// </summary>
        public string? Route { get; set; }
        /// <summary>
        /// The Controller/Method executed for this endpoint.
        /// </summary>
        public string? Action { get; set; }
        /// <summary>
        /// The qualified name of the method for this endpoint.
        /// </summary>
        public string? ControllerMethod { get; set; }
        /// <summary>
        /// The C# class returned by this API.
        /// </summary>
        public string? ReturnType { get; set; }
        /// <summary>
        /// The types of parameters in this method.
        /// </summary>
        public string[]? ParameterTypes { get; set; }
        /// <summary>
        /// The names of parameters in this method.
        /// </summary>
        public string[]? ParameterNames { get; set; }
        /// <summary>
        /// The attributes for each parameter.
        /// </summary>
        public Attribute[][]? ParameterAttributes { get; set; }
        /// <summary>
        /// If true, the endpoint requires a Bearer token for authorization.
        /// </summary>
        public bool AuthorizationRequired { get; set; }
    }
}
