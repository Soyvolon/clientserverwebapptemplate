﻿namespace ClientServerWebAppTemplate.Structures.Api;

/// <summary>
/// An error in the API.
/// </summary>
public class ApiError
{
    /// <summary>
    /// The error message.
    /// </summary>
    public string Error { get; set; } = "";
}

