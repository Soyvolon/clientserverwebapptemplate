﻿namespace ClientServerWebAppTemplate.Structures.Model;

/// <summary>
/// A helper class to hold a request refresh request.
/// </summary>
public class RefreshContext
{
    /// <summary>
    /// The refresh request call.
    /// </summary>
    public required Func<Task> RequestRefresh { get; init; }
}
