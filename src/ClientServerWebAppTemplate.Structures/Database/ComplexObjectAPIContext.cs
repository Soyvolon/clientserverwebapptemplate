﻿using Microsoft.EntityFrameworkCore;

namespace ClientServerWebAppTemplate.Structures.Database;
public class ComplexObjectAPIContext : DbContext
{
    public ComplexObjectAPIContext(DbContextOptions<ComplexObjectAPIContext> options)
            : base(options)
    {

    }

    /// <summary>
    /// Applies migrations to the database.
    /// </summary>
    /// <remarks>
    /// When using SQLite, this may have to be called twice,
    /// so we run the migration attempt twice.
    /// </remarks>
    /// <returns>A task for this action.</returns>
    public async Task ApplyMigrationsAsync()
    {
        for (int i = 0; i < 2; i++)
        {
            if (!(await Database.GetPendingMigrationsAsync()).Any())
            {
                return;
            }

            await Database.MigrateAsync();
            await SaveChangesAsync();
        }
    }
}
