﻿namespace ClientServerWebAppTemplate.Structures.Pagination;

/// <summary>
/// Indicates pagination data.
/// </summary>
/// <param name="Skip">How many entries to skip.</param>
/// <param name="Take">How many entries to take.</param>
public record PaginationRecord(int Skip, int Take);
