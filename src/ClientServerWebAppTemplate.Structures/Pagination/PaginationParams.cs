﻿using Microsoft.AspNetCore.Mvc;

namespace ClientServerWebAppTemplate.Structures.Pagination;

/// <summary>
/// Pagination query params.
/// </summary>
/// <param name="SkipBase">How many elements to skip before taking form the data.</param>
/// <param name="Start">Same as <paramref name="SkipBase"/>.</param>
/// <param name="TakeBase">How many elements to take from the data.</param>
/// <param name="Count">Same as <paramref name="TakeBase"/>.</param>
/// <param name="InstanceBase">The existing pagination instance, if applicable.</param>
public record PaginationParams(
    [FromQuery(Name = "skip")]
    int SkipBase = 0,
    [FromQuery(Name = "start")]
    int? Start = null,
    [FromQuery(Name = "take")]
    int TakeBase = PaginationParams.DEFAULT_PAGE_SIZE,
    [FromQuery(Name = "count")]
    int? Count = null,
    [FromQuery(Name = "instance")]
    Guid? InstanceBase = null)
{
    /// <summary>
    /// The default size of a single page for the search api.
    /// </summary>
    public const int DEFAULT_PAGE_SIZE = 15;

    /// <summary>
    /// Gets the skip value.
    /// </summary>
    /// <returns>An integer.</returns>
    public readonly int Skip = Start ?? SkipBase;

    /// <summary>
    /// Gets the take value.
    /// </summary>
    /// <returns>An integer.</returns>
    public readonly int Take = Count ?? TakeBase;

    /// <summary>
    /// The current pagination instance or <see cref="Guid.Empty"/>.
    /// </summary>
    public readonly Guid Instance = InstanceBase ?? Guid.Empty;
}
