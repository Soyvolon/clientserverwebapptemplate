﻿using ClientServerWebAppTemplate.Structures.Result;

namespace ClientServerWebAppTemplate.Structures.Pagination;

/// <summary>
/// Configures pagination data.
/// </summary>
/// <typeparam name="TData">The type of objects received from <paramref name="GetDataAsync"/></typeparam>
/// <param name="GetCountAsync">A method to get the total count of items.</param>
/// <param name="GetDataAsync">A method to get data between a range as defined in a <see cref="PaginationRecord"/>
/// object.</param>
/// <param name="OnDestroyAsync">An optional method to fire when the pagination service disposes
/// of an expired setup.</param>
public record PaginationSetup<TData>(
    Func<Task<int>> GetCountAsync,
    Func<PaginationRecord, Task<ActionResult<IEnumerable<TData>>>> GetDataAsync,
    Func<Guid, Task>? OnDestroyAsync = null
);
