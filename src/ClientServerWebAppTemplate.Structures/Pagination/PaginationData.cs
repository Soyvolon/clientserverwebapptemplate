﻿namespace ClientServerWebAppTemplate.Structures.Pagination;

/// <summary>
/// A class holding pagination data.
/// </summary>
/// <param name="results">The search result.</param>
/// <param name="start">The starting index.</param>
/// <param name="count">The number of results returned.</param>
/// <param name="total">The total number of results.</param>
/// <param name="next">A link to the next page of results.</param>
/// <param name="instance">The id of the saved instance data.</param>
/// <param name="expiresAt">The time at which the <paramref name="instance"/> expires
/// and pagination is no longer available for this request.</param>
public class PaginationData<T>(T[] results, int start, int count, int total,
    Guid? instance, string? next, DateTime? expiresAt)
{
    /// <summary>
    /// The array of data.
    /// </summary>
    public T[] Results { get; } = results;
    /// <summary>
    /// The starting index.
    /// </summary>
    public int Start { get; } = start;
    /// <summary>
    /// The number of elements in <see cref="Results"/>.
    /// </summary>
    public int Count { get; } = count;
    /// <summary>
    /// The total number of elements returned before pagination.
    /// </summary>
    public int Total { get; } = total;
    /// <summary>
    /// The pagination instance ID
    /// </summary>
    public Guid? Instance { get; } = instance;
    /// <summary>
    /// The url to the next page.
    /// </summary>
    public string? Next { get; set; } = next;
    /// <summary>
    /// The time at which the pagination instance expires.
    /// Gets reset every time a new request is made.
    /// </summary>
    public DateTime? ExpiresAt { get; } = expiresAt;
}
