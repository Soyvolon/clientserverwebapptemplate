namespace ClientServerWebAppTemplate.Structures.Util.Interfaces;

/// <summary>
/// An interface for things with an order.
/// </summary>
public interface IOrderable
{
    /// <summary>
    /// The order index.
    /// </summary>
    public int Order { get; set; }
}

/// <summary>
/// Extensions for <see cref="IOrderable"/>.
/// </summary>
public static class OrderableExtensions
{
    /// <summary>
    /// Sets the order value in the items to their index
    /// in an enumerable (i.e. list or array).
    /// </summary>
    /// <typeparam name="T">The type of orderable.</typeparam>
    /// <param name="items">The items to set the orders of.</param>
    public static void RecalculateOrder<T>(this IEnumerable<T> items)
        where T : IOrderable
    {
        var c = 0;
        foreach (var item in items)
        {
            item.Order = c++;
        }
    }
}
