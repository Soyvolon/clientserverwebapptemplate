﻿namespace ClientServerWebAppTemplate.Structures.Util.Interfaces;

/// <summary>
/// A sortable object.
/// </summary>
public interface ISortable
{
    /// <summary>
    /// Recalculate the order of an item at <paramref name="oldIndex"/> by moving it to <paramref name="newIndex"/>.
    /// </summary>
    /// <param name="oldIndex">The old index.</param>
    /// <param name="newIndex">The new index.</param>
    public void RecalculateOrder(int oldIndex, int newIndex);
}
