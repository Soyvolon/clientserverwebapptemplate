﻿using Microsoft.JSInterop;

namespace ClientServerWebAppTemplate.Structures.Util.Interfaces;

/// <summary>
/// An interface for components that require a heartbeat.
/// </summary>
public interface IHasHeartbeat
{
    /// <summary>
    /// When the JS triggers a heartbeat, this method returns a status.
    /// </summary>
    /// <returns>True if the component is alive, false if it is not.</returns>
    [JSInvokable]
    public Task<bool> HeartbeatAsync();
}
