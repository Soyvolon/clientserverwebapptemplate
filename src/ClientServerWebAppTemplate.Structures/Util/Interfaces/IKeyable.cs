namespace ClientServerWebAppTemplate.Structures.Util.Interfaces;

/// <summary>
/// An interface for objects with a key.
/// </summary>
/// <typeparam name="T">The type of key the object contains.</typeparam>
public interface IKeyable<T>
{
    /// <summary>
    /// Gets the key for this object.
    /// </summary>
    /// <returns>The key object.</returns>
    public T GetKey();
}
