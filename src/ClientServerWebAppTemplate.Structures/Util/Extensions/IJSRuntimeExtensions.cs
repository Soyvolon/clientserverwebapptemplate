using Microsoft.JSInterop;

namespace ClientServerWebAppTemplate.Structures.Util.Extensions;

/// <summary>
/// A set of extensions for executing local http requests
/// to the template helper app.
/// </summary>
public static class IJSRuntimeExtensions
{
    #region Local Storage
    /// <summary>
    /// Sets a value in the browser's local storage.
    /// </summary>
    /// <param name="runtime">The JS runtime.</param>
    /// <param name="key">The item key.</param>
    /// <param name="value">The value.</param>
    /// <returns>A task for this action.</returns>
    public static async Task SetLocalAsync(this IJSRuntime runtime, string key, string value)
        => await runtime.InvokeVoidAsync("localStorage.setItem", key, value);

    /// <summary>
    /// Gets a value from the browser's local storage.
    /// </summary>
    /// <param name="runtime">The JS runtime.</param>
    /// <param name="key">The key of the value to get.</param>
    /// <returns>The value.</returns>
    public static async Task<string> GetLocalAsync(this IJSRuntime runtime, string key)
        => await runtime.InvokeAsync<string>("localStorage.getItem", key);

    /// <summary>
    /// Removes a value from the browser's local storage.
    /// </summary>
    /// <param name="runtime">The JS runtime.</param>
    /// <param name="key">The key of the value to remove.</param>
    /// <returns>The value.</returns>
    public static async Task<string?> RemoveLocalAsync(this IJSRuntime runtime, string key)
        => await runtime.InvokeAsync<string>("localStorage.removeItem", key);
    #endregion

    #region Theme
    /// <summary>
    /// Gets the current theme.
    /// </summary>
    /// <param name="interop">The IJSRuntime instance.</param>
    /// <returns>The theme name.</returns>
    public static async Task<string> GetThemeAsync(this IJSRuntime interop)
    {
        const string method = "Util.getTheme";
        return await interop.InvokeAsync<string>(method);
    }

    /// <summary>
    /// Sets the current theme.
    /// </summary>
    /// <param name="interop">The IJSRuntime instance.</param>
    /// <param name="theme">The new theme.</param>
    /// <returns>A task for this action.</returns>
    public static async Task SetThemeAsync(this IJSRuntime interop, string? theme)
    {
        const string method = "Util.setTheme";
        await interop.InvokeVoidAsync(method, theme);
    }
    #endregion

    #region Util
    /// <summary>
    /// Triggers a client file download.
    /// </summary>
    /// <remarks>
    /// The file extension on disk must match the file extension used in <paramref name="fileName"/>.
    /// </remarks>
    /// <param name="interop">The IJSRuntime instance.</param>
    /// <param name="url">The local URL to the file in the wwwroot folder.</param>
    /// <param name="fileName">The name of the file when downloading.</param>
    /// <returns>A task for this action.</returns>
    public static async Task TriggerFileDownloadAsync(this IJSRuntime interop, string url,
        string fileName)
    {
        const string method = "Util.triggerFileDownload";
        await interop.InvokeVoidAsync(method, fileName, url);
    }

    /// <summary>
    /// Gets the scroll height of a container.
    /// </summary>
    /// <param name="interop">The IJSRuntime instance.</param>
    /// <param name="id">The ID of the container to get.</param>
    /// <returns>A string holding the scroll height.</returns>
    public static async Task<string> GetScrollHeight(this IJSRuntime interop, string id)
    {
        const string method = "Util.getScrollHeight";
        return await interop.InvokeAsync<string>(method, id);
    }
    #endregion
}
