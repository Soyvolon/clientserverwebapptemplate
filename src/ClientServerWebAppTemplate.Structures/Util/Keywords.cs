using System.Text.Json;
using System.Text.Json.Serialization;

namespace ClientServerWebAppTemplate.Structures.Util;

/// <summary>
/// Globals and other values.
/// </summary>
public static class Keywords
{
    /// <summary>
    /// The JSON options for the template builder.
    /// </summary>
    public static JsonSerializerOptions TemplateBuilderJsonOptions { get; set; } = new()
    {
        TypeInfoResolver = new DataObjectPolymorphicTypeResolver(),
        WriteIndented = false,
        PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower,
        ReferenceHandler = ReferenceHandler.Preserve
    };
}
