using System.Collections;

namespace ClientServerWebAppTemplate.Structures.Layout;
/// <summary>
/// Holds selection data for rows of data.
/// </summary>
public class RowSelectionData : IEnumerable<int>
{
    /// <summary>
    /// A sorted set of the selected indexes.
    /// </summary>
    private SortedSet<int> SelectedIndexes { get; set; } = [];

    /// <summary>
    /// The total number of elements available.
    /// </summary>
    public int Range { get; set; } = 0;

    /// <summary>
    /// If true, select everything.
    /// </summary>
    public bool SelectedAll
    {
        get => SelectedIndexes.Count == Range;
        set
        {
            if (value)
            {
                for (int i = 0; i < Range; i++)
                    Select(i);
            }
            else
            {
                Clear();
            }
        }
    }

    /// <summary>
    /// Selects an index.
    /// </summary>
    /// <param name="index">The index to select.</param>
    public void Select(int index)
    {
        SelectedIndexes.Add(index);
    }

    /// <summary>
    /// Deselects an index.
    /// </summary>
    /// <param name="index">The index to deselect.</param>
    public void Deselect(int index)
    {
        SelectedIndexes.Remove(index);
    }

    /// <summary>
    /// Gets the current selections.
    /// </summary>
    /// <returns>The array of indexes for selected elements.</returns>
    public int[] GetSelections()
        => [.. SelectedIndexes];

    /// <summary>
    /// Clears all current selections.
    /// </summary>
    public void Clear()
        => SelectedIndexes.Clear();

    /// <summary>
    /// Enumerates through the selected indexes.
    /// </summary>
    /// <returns>The enumerator</returns>
    public IEnumerator<int> GetEnumerator()
        => SelectedIndexes.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator()
        => GetEnumerator();

    /// <summary>
    /// Gets or sets an index.
    /// </summary>
    /// <param name="key">The index.</param>
    /// <returns>True if the index is set.</returns>
    public bool this[int key]
    {
        get => SelectedIndexes.Contains(key);
        set
        {
            if (value)
                Select(key);
            else Deselect(key);
        }
    }
}
