﻿using Microsoft.AspNetCore.Components.Web;

namespace ClientServerWebAppTemplate.Structures.Events;

public class OnPressEventArgs : EventArgs
{
    public string Code { get; set; }

    public bool ShiftKey { get; set; }

    public bool CtrlKey { get; set; }

    public bool AltKey { get; set; }

    public bool MetaKey { get; set; }

    public OnPressEventArgs()
        : this("", false, false, false, false) { }

    public OnPressEventArgs(KeyboardEventArgs args)
        : this(args.Code, args.ShiftKey, args.CtrlKey, args.AltKey, args.MetaKey) { }

    public OnPressEventArgs(string code, bool shiftKey, bool ctrlKey, bool altKey, bool metaKey)
    {
        Code = code;
        ShiftKey = shiftKey;
        CtrlKey = ctrlKey;
        AltKey = altKey;
        MetaKey = metaKey;
    }

    public override bool Equals(object? obj)
    {
        if (obj is null)
            return false;

        if (obj is OnPressEventArgs args)
            return GetHashCode().Equals(args.GetHashCode());

        return false;
    }

    public override int GetHashCode()
    {
        HashCode hc = new();
        hc.Add(Code);
        hc.Add(ShiftKey);
        hc.Add(CtrlKey);
        hc.Add(AltKey);
        hc.Add(MetaKey);
        return hc.ToHashCode();
    }
}
