﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.Structures.Events;

/// <summary>
/// The arguments for opening a new display menu.
/// </summary>
/// <param name="menu">The menu to render.</param>
/// <param name="id">The id of the menu.</param>
/// <param name="lockoutNavigation">If true, prevent navigation from leaving the page until
/// the menu is closed.</param>
/// <param name="allowEscapeClose">If true, allow the escape key to close the menu.</param>
public class DisplayMenuEventArgs(RenderFragment menu, string id, bool lockoutNavigation,
    bool allowEscapeClose) : EventArgs
{
    /// <summary>
    /// The menu to render.
    /// </summary>
    public RenderFragment Menu { get; set; } = menu;
    /// <summary>
    /// The ID of the menu.
    /// </summary>
    public string Id { get; set; } = id;
    /// <summary>
    /// If true, lockout navigation until the menu is closed.
    /// </summary>
    public bool LockoutNavigation { get; set; } = lockoutNavigation;
    /// <summary>
    /// If true, allow the escape button to close the popup.
    /// </summary>
    public bool AllowEscapeClose { get; set; } = allowEscapeClose;
}