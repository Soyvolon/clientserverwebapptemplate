﻿using ClientServerWebAppTemplate.Structures.Pagination;

using System.Collections.Concurrent;

namespace ClientServerWebAppTemplate.Server.Services.Pagination;

/// <summary>
/// A class for the root pagination services.
/// </summary>
public class PaginationService<T>() : IPaginationService<T>
    where T : class
{
    /// <summary>
    /// The data instances for this pagination.
    /// </summary>
    protected ConcurrentDictionary<Guid, (PaginationSetup<T> config, TimeSpan expr)> Instances { get; init; } = new();
    /// <summary>
    /// Timers running until the instance is expired.
    /// </summary>
    protected ConcurrentDictionary<Guid, Timer> ExpirationTimers { get; init; } = new();

    /// <inheritdoc/>
    public Guid CreateInstance(PaginationSetup<T> setup, TimeSpan? exprIn)
    {
        var key = Guid.NewGuid();
        var expire = exprIn ?? IPaginationService<T>.ExpirationDefault;
        var timer = new Timer(OnExpiration, key, expire, Timeout.InfiniteTimeSpan);

        ExpirationTimers[key] = timer;
        Instances[key] = (setup, expire);

        return key;
    }

    /// <inheritdoc/>
    public async Task<PaginationData<T>> GetRecordAsync(Guid key, int start, int count)
    {
        if (!Instances.TryGetValue(key, out var instance))
            throw new IPaginationService<T>.PaginationInstanceExpiredException();

        var size = await instance.config.GetCountAsync();
        var dataRes = await instance.config.GetDataAsync(new(start, count));
        if (!dataRes.GetResult(out var err, out var data))
            throw new IPaginationService<T>.PaginationInstanceDataRetrievalFailed(string.Join("; ", err));

        var nextExpr = DateTime.Now + instance.expr;

        if (!ExpirationTimers.TryGetValue(key, out var timer))
        {
            timer = new(OnExpiration, key, instance.expr, Timeout.InfiniteTimeSpan);
            ExpirationTimers[key] = timer;
        }
        else
        {
            timer.Change(instance.expr, Timeout.InfiniteTimeSpan);
        }

        T[] dataArray = [.. data];

        var next = GetQueryParams(key, start, dataArray.Length, size);

        var record = new PaginationData<T>(dataArray, start, dataArray.Length,
            size, key, next, nextExpr);

        return record;
    }

    /// <summary>
    /// Runs when a timer expires.
    /// </summary>
    /// <param name="key">The key object (a <see cref="Guid"/>).</param>
    protected virtual void OnExpiration(object? key)
    {
        if (key is not Guid id)
            return;

        _ = Instances.Remove(id, out _);
        _ = ExpirationTimers.Remove(id, out _);
    }

    /// <summary>
    /// Gets the next part of the query.
    /// </summary>
    /// <param name="key">The instance key.</param>
    /// <param name="start">The starting index.</param>
    /// <param name="count">The number of elements to get.</param>
    /// <param name="length">The total length.</param>
    /// <returns>The query params for a request.</returns>
    protected virtual string? GetQueryParams(Guid key, int start, int count, int length)
    {
        if (start + count >= length)
            return null;

        return $"?instance={key}&start={start + count}&count={count}";
    }
}
