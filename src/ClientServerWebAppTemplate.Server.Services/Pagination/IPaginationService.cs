﻿using ClientServerWebAppTemplate.Structures.Pagination;

namespace ClientServerWebAppTemplate.Server.Services.Pagination;

/// <summary>
/// A service for handling app wide pagination data.
/// </summary>
public interface IPaginationService<T>
    where T : class
{
    /// <summary>
    /// The default expiration time for a pagination record.
    /// </summary>
    public static readonly TimeSpan ExpirationDefault = TimeSpan.FromMinutes(5);

    /// <summary>
    /// Creates a new pagination instance.
    /// </summary>
    /// <param name="data">The data for the instance.</param>
    /// <param name="exprIn">Optional param indicating the time till expiration. If
    /// not provided, <see cref="ExpirationDefault"/> is used.</param>
    /// <returns>The key of the instance data.</returns>
    public Guid CreateInstance(PaginationSetup<T> data, TimeSpan? exprIn);

    /// <summary>
    /// Gets a new pagination record for the query.
    /// </summary>
    /// <param name="key">The key to get an instance for.</param>
    /// <param name="start">The starting index.</param>
    /// <param name="count">The total number of elements to receive.</param>
    /// <exception cref="PaginationInstanceExpiredException" />
    /// <returns>A <see cref="PaginationData{T}"/> if the key exists, 
    /// otherwise an exception is thrown with an optional query
    /// to restart the pagination.</returns>
    public Task<PaginationData<T>> GetRecordAsync(Guid key, int start, int count);

    /// <summary>
    /// An exception for expired pagination instances.
    /// </summary>
    public class PaginationInstanceExpiredException : Exception { }
    /// <summary>
    /// An exception for when data retrieval from the database fails.
    /// </summary>
    public class PaginationInstanceDataRetrievalFailed : Exception
    {
        /// <inheritdoc/>
        public PaginationInstanceDataRetrievalFailed(string message)
            : base(message) { }
    }
}
