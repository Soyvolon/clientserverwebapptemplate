using ClientServerWebAppTemplate.Server.Controllers;
using ClientServerWebAppTemplate.Structures.Http;
using ClientServerWebAppTemplate.Structures.Http.Services;

using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using System.Reflection;
using System.Text;

namespace ClientServerWebAppTemplate.UI.Services.CodeGenerator;
public static class ApiServiceGenerator
{
    private const string CLASS_TAB = @"        ";
    private const string METHOD_TAB = @"            ";
    private const string TAB = @"    ";
    private const string ENTRY_NAMESPACE = "ClientServerWebAppTemplate.UI.Services";

    private const string INTERFACE_TAG = "{{interface}}";
    private const string CLASS_TAG = "{{class}}";
    private const string USING_TAG = "{{using}}";

    private const string BEARER_PARAM_NAME = "bearerToken";

    private static readonly string[] defaultUsingStatements = [
        "System.Text.Json",
        "System",
        "System.Linq",
        "System.Threading.Tasks",
        "System.Collections.Generic",
        "System.Text",
        "Microsoft.AspNetCore.Components.Authorization",
        "ClientServerWebAppTemplate.Structures.Result"
    ];

    private static readonly string[] defaultServiceLoaderUsingStatements = [
        "Microsoft.Extensions.DependencyInjection"
    ];

    public static void Run(string[] args)
    {
        Console.WriteLine("Starting API source generator...");

        var source = Assembly.GetAssembly(typeof(ClientServerWebAppTemplate.Server.Startup))
            ?? throw new Exception("No API Assembly found.");

        Console.WriteLine("Reading directory parameter...");

        var destinationSuffix = args[0].Trim(['\'', '"']);

        var executionFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        string destination = Path.GetFullPath(Path.Combine(executionFolder ?? "gen", destinationSuffix));

        Console.WriteLine("Cleaning output path...");

        if (Directory.Exists(destination))
            Directory.Delete(destination, true);
        var dirInfo = Directory.CreateDirectory(destination);

        Console.WriteLine("Exporting generated files to: " + dirInfo.FullName);

        GenerateServiceMethods(args, source, destination);

        Console.WriteLine("API source generation complete.");
    }

    private static void GenerateServiceMethods(string[] args, Assembly apiAssembly, string destination)
    {
        var endpoints = GetAllEndpoints(args);
        var endpointGroups = GetEndpointGroupsByNamespace(endpoints);

        List<(string, string, string)> interfaces = [];
        foreach (var endpointGroup in endpointGroups)
        {
            var (folder, file, code) = GetEndpointGroupCode(ENTRY_NAMESPACE, endpointGroup.Key, endpointGroup.Value,
                out var interfaceName, out var className, out var namespaceName);

            interfaces.Add((interfaceName, className, namespaceName));

            CreateFile(destination, folder, file, code);
        }

        var (setupFolder, setupFile, setupCode) = GetServiceLoaderFile(ENTRY_NAMESPACE, interfaces);

        CreateFile(destination, setupFolder, setupFile, setupCode);
    }

    private static void CreateFile(string destination, string folder, string file, string code)
    {
        var folderPath = Path.Combine(destination, folder);
        var filePath = Path.Combine(folderPath, file);

        Directory.CreateDirectory(folderPath);
        File.WriteAllText(filePath, code);
    }

    private static List<EndpointData> GetAllEndpoints(string[] args)
    {
        using var host = ClientServerWebAppTemplate.Server.Program.CreateHostBuilder(args).Build();
        host.RunAsync();

        using var scope = host.Services.CreateScope();
        var endpointSources = scope.ServiceProvider.GetRequiredService<IEnumerable<EndpointDataSource>>();
        var info = new InfoController(endpointSources);

        var endpoints = info.GetEndpointDataList();

        host.StopAsync();

        return endpoints;
    }

    private static Dictionary<string, List<EndpointData>> GetEndpointGroupsByNamespace(List<EndpointData> endpoints)
    {
        Dictionary<string, List<EndpointData>> dict = new();
        foreach (var endpoint in endpoints)
        {
            var namespaceStr = endpoint.ControllerMethod?.Split(':')[0] ?? "";

            if (!dict.TryGetValue(namespaceStr, out List<EndpointData>? list))
                dict[namespaceStr] = list = [];

            list.Add(endpoint);
        }

        return dict;
    }

    private static (string folder, string file, string code) GetEndpointGroupCode(string namespaceRoot, string namespaceStr,
        List<EndpointData> endpoints, out string interfaceDefinition, out string classDefinition,
        out string namespaceDefinition)
    {

        HashSet<string> usingStatements = new(defaultUsingStatements);

        StringBuilder code = new();
        code.AppendLine(@"// <auto-generated/>");
        code.AppendLine("{{using}}");

        code.AppendLine();

        var localNamespace = Path.GetFileNameWithoutExtension(namespaceStr);
        var rootParts = namespaceRoot.Split('.');
        foreach (var part in rootParts)
            localNamespace = localNamespace.Replace(part, string.Empty);
        localNamespace = localNamespace.Trim('.');

        var namespaceFullName = $"{namespaceRoot}.{localNamespace}";

        code.AppendFormat("namespace {0}\n", namespaceFullName);
        code.AppendLine("{");

        code.AppendLine(INTERFACE_TAG);
        code.AppendLine(CLASS_TAG);

        code.AppendLine("}");

        var className = Path.GetExtension(namespaceStr)
            .Trim('.').Replace("Controller", "Service");

        var classBuilder = new StringBuilder();
        classBuilder.AppendFormat("    public class {0} : {1}, I{0}\n", className,
            nameof(HTTPCallService));

        var baseClassNamespace = typeof(HTTPCallService).Namespace;
        if (!string.IsNullOrWhiteSpace(baseClassNamespace))
            usingStatements.Add(baseClassNamespace);

        classBuilder.AppendLine("    {");

        classBuilder.AppendFormat("{0}public {1}({2} provider) : base(provider)", CLASS_TAB, className,
            nameof(IServiceProvider));
        classBuilder.AppendLine(" { }");

        List<(string name, string returnType, string paramsStr)> methods = [];
        foreach (var endpoint in endpoints)
        {
            var methodNameStart = endpoint.ControllerMethod!.IndexOf(':') + 1;
            var methodName = endpoint.ControllerMethod[methodNameStart..];

            if (!methodName.EndsWith("Async"))
            {
                methodName += "Async";
            }

            string? methodReturnType;
            string methodReturnDeclaration;
            if (endpoint.ReturnType is not null)
            {
                methodReturnType = Path.GetExtension(endpoint.ReturnType).Trim('.');

                var typeIndex = endpoint.ReturnType.Length - methodReturnType.Length - 1;
                var typeNamespace = endpoint.ReturnType[..typeIndex];

                methodReturnDeclaration = string.Format("async Task<{0}<{1}>>", nameof(ActionResult), methodReturnType);
                usingStatements.Add(typeNamespace);
            }
            else
            {
                methodReturnType = null;
                methodReturnDeclaration = string.Format("async Task<{0}>", nameof(ActionResult));
            }

            string methodArgs = "";

            bool hasParams = endpoint.ParameterNames is not null
                    && endpoint.ParameterTypes is not null
                    && endpoint.ParameterNames.Length == endpoint.ParameterTypes.Length;
            if (hasParams)
            {
                for (int i = 0; i < endpoint.ParameterNames!.Length; i++)
                {
                    if (i != 0)
                        methodArgs += ", ";

                    methodArgs += $"{endpoint.ParameterTypes![i]} {endpoint.ParameterNames[i]}";
                }
            }

            // Start of method
            classBuilder.AppendFormat("{0}public {1} {2}({3})\n", CLASS_TAB, methodReturnDeclaration, methodName, methodArgs);
            classBuilder.AppendLine(CLASS_TAB + "{");

            List<string> parameters = [];
            if (endpoint.ParameterNames?.Length > 0)
            {
                for (int i = 0; i < endpoint.ParameterNames.Length; i++)
                {
                    string name = endpoint.ParameterNames[i];
                    string type = "query";

                    Attribute? attr = endpoint.ParameterAttributes?[i].FirstOrDefault() ?? null;
                    if (attr is not null)
                    {
                        type = attr switch
                        {
                            FromBodyAttribute => "body",
                            FromHeaderAttribute => "header",
                            FromQueryAttribute => "query",
                            FromFormAttribute => "form",
                            FromRouteAttribute => "route",
                            _ => "query"
                        };
                    }

                    parameters.Add($@"new {typeof(HttpParameter).FullName}(""{name}"", ""{type}"", {name})");
                }
            }

            classBuilder.AppendFormat(@"{0}var action = await {1}(""{2}"", ""{3}"", {4}, {5});",
                METHOD_TAB,
                nameof(HTTPCallService.ExecuteRestRequestAsync),
                endpoint.Method,
                endpoint.Route,
                endpoint.AuthorizationRequired ? "true" : "false",
                hasParams ? "null"
                    : $"\n{METHOD_TAB}    {string.Join($",\n{METHOD_TAB}    ", parameters)}"
            );

            classBuilder.AppendLine();
            classBuilder.AppendFormat(@"{0}if (!action.GetResult(out var err, out var res))",
                METHOD_TAB);
            classBuilder.AppendLine();
            classBuilder.AppendFormat(@"{0}{1}return new(false, err, null);",
                METHOD_TAB, TAB);
            classBuilder.AppendLine();
            classBuilder.AppendLine();

            if (methodReturnType is not null)
            {
                classBuilder.AppendFormat("{0}if (res is null)", METHOD_TAB);
                classBuilder.AppendLine();

                classBuilder.AppendFormat(@"{0}{1}return new(false, [""No result data to parse.""], null);",
                    METHOD_TAB, TAB);
                classBuilder.AppendLine();
                classBuilder.AppendLine();

                classBuilder.AppendLine($"{METHOD_TAB}try");
                classBuilder.AppendLine($"{METHOD_TAB}{{");

                classBuilder.AppendFormat("{0}{1}var data = JsonSerializer.Deserialize<{2}>(res, {3});",
                    METHOD_TAB, TAB, methodReturnType, nameof(HTTPCallService.JsonOptions));
                classBuilder.AppendLine();

                classBuilder.AppendFormat("{0}{1}return new(true, null, data);",
                    METHOD_TAB, TAB);
                classBuilder.AppendLine();

                classBuilder.AppendLine($"{METHOD_TAB}}}");
                classBuilder.AppendLine($"{METHOD_TAB}catch ({nameof(Exception)} ex)");
                classBuilder.AppendLine($"{METHOD_TAB}{{");
                classBuilder.AppendLine($"{METHOD_TAB}{TAB}const string errMsg = \"An unexpected error occurred.\";");
                classBuilder.AppendLine($"{METHOD_TAB}{TAB}return new(false, [errMsg, ex.Message], null);");
                classBuilder.AppendLine($"{METHOD_TAB}}}");
            }
            else
            {
                classBuilder.AppendLine($"{METHOD_TAB}return new(true);");
            }

            // End of method
            classBuilder.AppendLine(CLASS_TAB + "}");

            methods.Add((methodName, methodReturnDeclaration, methodArgs));
        }

        classBuilder.AppendLine("    }");

        var interfaceBuilder = new StringBuilder();
        var interfaceName = $"I{className}";

        interfaceBuilder.AppendFormat("    public interface {0}\n", interfaceName);
        interfaceBuilder.AppendLine("    {");

        // Interface definition
        foreach (var method in methods)
        {
            var returnType = method.returnType.Replace("async", "").Trim();

            interfaceBuilder.AppendFormat("{0}public {1} {2}({3});",
                CLASS_TAB, returnType, method.name, method.paramsStr);

            interfaceBuilder.AppendLine();
        }

        interfaceBuilder.AppendLine("    }");

        StringBuilder usingBuilder = new();
        foreach (var usingStatement in usingStatements)
        {
            usingBuilder.AppendFormat("using {0};\n", usingStatement);
        }

        var final = code.ToString()
            .Replace(INTERFACE_TAG, interfaceBuilder.ToString())
            .Replace(CLASS_TAG, classBuilder.ToString())
            .Replace(USING_TAG, usingBuilder.ToString());

        namespaceDefinition = namespaceFullName;
        interfaceDefinition = interfaceName;
        classDefinition = className;

        var folder = namespaceFullName
            .Replace(namespaceRoot, "")
            .Trim('.')
            .Replace('.', '\\')
            ?? "";

        var file = className + ".g.cs";
        return (folder, file, final);
    }

    private static (string folder, string file, string code) GetServiceLoaderFile(string namespaceRoot,
        List<(string interfaceName, string className, string namespaceName)> codeGroups)
    {
        var code = new StringBuilder();
        code.AppendLine(@"// <auto-generated/>");
        code.AppendLine(USING_TAG);

        HashSet<string> usingStatements = new(defaultServiceLoaderUsingStatements);

        code.AppendLine();

        code.AppendFormat("namespace {0}", namespaceRoot);
        code.AppendLine();
        code.AppendLine("{");

        code.AppendLine("    internal static class ServiceLoaderBase");
        code.AppendLine("    {");
        code.AppendFormat("{0}internal static IServiceCollection LoadBaseServices(this IServiceCollection collection)", CLASS_TAB);
        code.AppendLine();
        code.AppendLine(CLASS_TAB + "{");

        code.AppendFormat("{0}collection.AddHttpClient();", METHOD_TAB);
        code.AppendLine();

        code.AppendLine();
        code.AppendFormat("{0}// Dynamic service registration", METHOD_TAB);
        code.AppendLine();

        foreach (var (interfaceName, className, namespaceName) in codeGroups)
        {
            _ = usingStatements.Add(namespaceName);

            code.AppendFormat("{0}collection.AddScoped<{1}, {2}>();", METHOD_TAB, interfaceName, className);
            code.AppendLine();
        }

        code.AppendLine();
        code.AppendFormat("{0}return collection;", METHOD_TAB);
        code.AppendLine();

        code.AppendLine(CLASS_TAB + "}");
        code.AppendLine("    }");
        code.AppendLine("}");

        StringBuilder usingBuilder = new();
        foreach (var usingStatement in usingStatements)
        {
            usingBuilder.AppendFormat("using {0};", usingStatement);
            usingBuilder.AppendLine();
        }

        var final = code.ToString()
            .Replace(USING_TAG, usingBuilder.ToString());

        return ("", "ServiceLoader.g.cs", final);
    }
}
