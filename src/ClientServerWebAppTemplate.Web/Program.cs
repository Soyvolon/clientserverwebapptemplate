using Microsoft.Extensions.Logging.EventLog;

using System.Reflection;

namespace ClientServerWebAppTemplate.Web;

public class Program
{
    public static async Task Main(string[] args)
    {
        var host = CreateHostBuilder(args);
        await host.Build().RunAsync();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
#pragma warning disable CA1416 // Validate platform compatibility
        => Host.CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(config =>
            {
                var path = Path.Join(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                    "Config", "website_config.json");

                config.AddJsonFile(path);
            })
            .ConfigureServices((hostContext, services) =>
            {
                try
                {
                    services.Configure<EventLogSettings>(settings =>
                    {
                        settings.SourceName = "ClientServerWebAppTemplate Log";
                    });
                }
                catch { /* not on windows */ }
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            });
#pragma warning restore CA1416 // Validate platform compatibility

}