using ClientServerWebAppTemplate.UI.Services;

using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.AspNetCore.HttpOverrides;

using System.Collections.Concurrent;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text.Json;
using System.Text.Json.Nodes;

namespace ClientServerWebAppTemplate.Web;

public class Startup
{
    private const string LOGIN_STATE_COOKIE = "login_state";

    private static TimeSpan TimeoutExpiration { get; } = TimeSpan.FromSeconds(60);
    private static ConcurrentDictionary<string, (ClaimsIdentity identity, Timer timer)> LoginKeys { get; } = new();

    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
        // Add services to the container.
        services.AddRazorComponents()
            .AddInteractiveServerComponents()
            .AddInteractiveWebAssemblyComponents();

        services.AddCascadingAuthenticationState()
            .AddControllers();

        services.AddHsts(options =>
        {
            options.Preload = true;
            options.IncludeSubDomains = true;
            options.MaxAge = TimeSpan.FromDays(365);
        });

        services.AddAuthentication(options =>
        {
            options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
            options.DefaultChallengeScheme = "default";
        })
            .AddCookie()
            .AddOpenIdConnect("default", options =>
            {
                options.Authority = Configuration["Accounts:Authority"] ?? "";
                options.ResponseType = "code";

                options.CallbackPath = new PathString("/authorization-code/default-callback"); // local auth endpoint
                options.AccessDeniedPath = new PathString("/"); // return home

                var clientId = Configuration["Accounts:ClientId"] ?? "";
                options.ClientId = clientId;
                options.ClientSecret = Configuration["Accounts:ClientSecret"] ?? "";

                options.Scope.Add("email");
                options.Scope.Add("profile");
                options.Scope.Add("roles");

                options.SaveTokens = false;
#if DEBUG
                options.RequireHttpsMetadata = false;
#endif

                options.UseTokenLifetime = false;

                options.CorrelationCookie = new()
                {
                    IsEssential = true,
                    SameSite = SameSiteMode.None,
                    SecurePolicy = CookieSecurePolicy.Always
                };

                options.Events.OnTicketReceived = (opt) =>
                {
                    if (opt is not TicketReceivedContext ctx)
                        return Task.CompletedTask;

                    if (!ctx.Request.Cookies.TryGetValue(LOGIN_STATE_COOKIE, out var state))
                        return Task.CompletedTask;

                    ctx.Response.Cookies.Delete(LOGIN_STATE_COOKIE);

                    if (!LoginKeys.TryRemove(state, out var keys))
                        return Task.CompletedTask;

                    ctx.Principal?.AddIdentity(keys.identity);

                    return Task.CompletedTask;
                };

                options.Events.OnTokenValidated = (opt) =>
                {
                    if (opt is not TokenValidatedContext ctx)
                        return Task.CompletedTask;

                    if (!ctx.Request.Cookies.TryGetValue(LOGIN_STATE_COOKIE, out var state))
                        return Task.CompletedTask;

                    var token = ctx.TokenEndpointResponse?.AccessToken;
                    var refresh = ctx.TokenEndpointResponse?.RefreshToken;
                    var id = ctx.TokenEndpointResponse?.IdToken;

                    var ident = new ClaimsIdentity([
                        new Claim("access_token", token ?? ""),
                        new Claim("refresh_token", refresh ?? ""),
                        new Claim("id_token", id ?? "")
                    ]);

                    // Add roles
                    var tokenHandler = new JwtSecurityTokenHandler();
                    var jwt = tokenHandler.ReadJwtToken(token);

                    var json = jwt.Payload["resource_access"];

                    if (json is JsonElement element)
                    {
                        var node = JsonNode.Parse(element.ToString());

                        foreach (var part in element.EnumerateObject())
                        {
                            if (part.Value.TryGetProperty("roles", out var elem))
                            {
                                var name = part.Name;
                                if (name == clientId)
                                    name = "app";

                                var roles = elem.Deserialize<string[]>();

                                if (roles is not null)
                                {
                                    foreach (var role in roles)
                                        if (role is not null)
                                            ident.AddClaim(new(ClaimTypes.Role, $"{name}:{role}"));
                                }
                            }
                        }
                    }

                    var loginTimer = new Timer((s) =>
                    {
                        if (s is not string key)
                            return;

                        if (!LoginKeys.TryRemove(key, out var value))
                            return;

                        value.timer.Dispose();
                    }, state, TimeoutExpiration, Timeout.InfiniteTimeSpan);

                    LoginKeys.TryAdd(state, (ident, loginTimer));

                    return Task.CompletedTask;
                };

                options.Events.OnRedirectToIdentityProvider = (opt) =>
                {
                    if (opt is not RedirectContext ctx)
                        return Task.CompletedTask;

                    var state = Guid.NewGuid().ToString();

                    ctx.Response.Cookies.Delete(LOGIN_STATE_COOKIE);
                    ctx.Response.Cookies.Append(LOGIN_STATE_COOKIE, state);

                    return Task.CompletedTask;
                };
            });

        services.AddAuthorizationBuilder()
            .AddPolicy("Templates", p => p.RequireClaim("template-builder"));

        services.LoadUIServices(Configuration);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseWebAssemblyDebugging();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseForwardedHeaders(new ForwardedHeadersOptions()
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });

        app.UseStaticFiles();

        app.UseRouting();

        app.UseAntiforgery();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            //endpoints.MapBlazorHub();
            //endpoints.MapFallbackToPage("/_Host");

            endpoints.MapRazorComponents<App>()
                .AddInteractiveServerRenderMode()
                .AddInteractiveWebAssemblyRenderMode()
                .AddAdditionalAssemblies(typeof(UI._Imports).Assembly);
        });
    }
}