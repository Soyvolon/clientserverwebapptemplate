#See https://aka.ms/customizecontainer to learn how to customize your debug container and how Visual Studio uses this Dockerfile to build your images for faster debugging.

ARG netversion=8.0

# do setup for the actual app.
FROM mcr.microsoft.com/dotnet/aspnet:$netversion AS base
WORKDIR /app

# Install/restore dependencies.
FROM mcr.microsoft.com/dotnet/sdk:$netversion AS prebuild
ARG netversion
ARG BUILD_CONFIGURATION=Release

# copy projects for restore
WORKDIR /base
COPY ["src/ClientServerWebAppTemplate.Web/ClientServerWebAppTemplate.Web.csproj", "src/ClientServerWebAppTemplate.Web/"]
COPY ["src/ClientServerWebAppTemplate.UI/ClientServerWebAppTemplate.UI.csproj", "src/ClientServerWebAppTemplate.UI/"]
COPY ["src/ClientServerWebAppTemplate.UI/ClientServerWebAppTemplate.UI.Framework.csproj", "src/ClientServerWebAppTemplate.UI.Framework/"]
COPY ["src/ClientServerWebAppTemplate.UI.Services/ClientServerWebAppTemplate.UI.Services.csproj", "src/ClientServerWebAppTemplate.UI.Services/"]
COPY ["src/ClientServerWebAppTemplate.Structures/ClientServerWebAppTemplate.Structures.csproj", "src/ClientServerWebAppTemplate.Structures/"]
COPY ["src/ClientServerWebAppTemplate.Structures/ClientServerWebAppTemplate.Structures.SQL.csproj", "src/ClientServerWebAppTemplate.Structures.SQL/"]
COPY ["src/ClientServerWebAppTemplate.UI.WASM/ClientServerWebAppTemplate.UI.WASM.csproj", "src/ClientServerWebAppTemplate.UI.WASM/"]

COPY ["src/ClientServerWebAppTemplate.Server/ClientServerWebAppTemplate.Server.csproj", "src/ClientServerWebAppTemplate.Server/"]
COPY ["src/ClientServerWebAppTemplate.UI.Services.CodeGenerator/ClientServerWebAppTemplate.UI.Services.CodeGenerator.csproj", "src/ClientServerWebAppTemplate.UI.Services.CodeGenerator/"]

RUN dotnet restore "./src/ClientServerWebAppTemplate.UI.Services.CodeGenerator/ClientServerWebAppTemplate.UI.Services.CodeGenerator.csproj"

# copy all source files
COPY . .

# build and run code generator
WORKDIR "/base/src/ClientServerWebAppTemplate.UI.Services.CodeGenerator"
RUN dotnet build "./ClientServerWebAppTemplate.UI.Services.CodeGenerator.csproj" -o "./../gensrc"
RUN dotnet "./../gensrc/ClientServerWebAppTemplate.UI.Services.CodeGenerator.dll" "ClientServerWebAppTemplate.UI.Services/gen"

# restore the base project
WORKDIR /base
RUN dotnet restore "./src/ClientServerWebAppTemplate.Web/ClientServerWebAppTemplate.Web.csproj"

# get node for css
FROM node:18 AS nodebuild
ARG netversion
COPY --from=prebuild /base /base

# run webpack
WORKDIR /base
RUN npm install
RUN npx webpack --mode=production --config css/webpack.config.js

# go back to .net
FROM prebuild AS build
COPY --from=nodebuild /base /base

WORKDIR "/base/src"
RUN find . -maxdepth 2 -ls

# build the project
WORKDIR "/base/src/ClientServerWebAppTemplate.Web"
RUN dotnet build "ClientServerWebAppTemplate.Web.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/build

# Run postcss commands.
FROM nodebuild AS postbuild
COPY --from=build /base /base

WORKDIR "/base/src/ClientServerWebAppTemplate.UI"
RUN npx postcss obj/Release/net${netversion}/scopedcss/bundle/ClientServerWebAppTemplate.UI.styles.css -o wwwroot/css/scoped/project.min.css --verbose
RUN npx postcss obj/Release/net${netversion}/scopedcss/projectbundle/ClientServerWebAppTemplate.UI.bundle.scp.css -o wwwroot/css/scoped/bundle.min.css --verbose
RUN npx postcss wwwroot/css/app.css -o wwwroot/css/app.min.css --verbose

# back to .net
FROM build AS manifest-build
COPY --from=postbuild /base /base

# re-build the UI for the manifest files.
WORKDIR "/base/src/ClientServerWebAppTemplate.UI"
RUN dotnet build "ClientServerWebAppTemplate.UI.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/build

# then publish the project
FROM manifest-build AS publish

WORKDIR "/base/src/ClientServerWebAppTemplate.Web"
RUN dotnet publish "ClientServerWebAppTemplate.Web.csproj" -p:SKIP_PREPOST_COMMANDS=True -c Release -o /app/publish -p:UseAppHost=false

# and set entrypoint
FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "ClientServerWebAppTemplate.Web.dll"]