using ClientServerWebAppTemplate.UI.Services.Server.Controllers;
using ClientServerWebAppTemplate.UI.Services.Server.Controllers.v1.Account;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Pages;

public partial class Index
{
    [Inject]
    public required IUserService UserService { get; set; }
    [Inject]
    public required IInfoService InfoService { get; set; }

    public string OutputText { get; set; } = "No Output";

    protected async Task TestNoAuthAPIAsync()
    {
        try
        {
            var res = await InfoService.ListAllEndpointsAsync();

            if (res.GetResult(out var err, out var data))
                OutputText = string.Join(", ", data.Select(e => e.ControllerMethod));
            else
                OutputText = string.Join(", ", err);
        }
        catch (Exception ex)
        {
            OutputText = "Errored: " + ex.Message;
        }
    }

    protected async Task TestAuthAPIAsync()
    {
        try
        {
            var res = await UserService.GetUserSubscriptionsAsync();

            if (res.GetResult(out var err, out var data))
                OutputText = string.Join(", ", data);
            else
                OutputText = string.Join(", ", err);
        }
        catch (Exception ex)
        {
            OutputText = "Errored: " + ex.Message;
        }
    }
}