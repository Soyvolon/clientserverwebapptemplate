﻿using ClientServerWebAppTemplate.Structures.Events;
using ClientServerWebAppTemplate.Structures.Util.Extensions;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace ClientServerWebAppTemplate.UI.Framework.Handlers;

public partial class PageInteractionHandler : ComponentBase, IDisposable
{
    private bool disposedValue;
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public required IJSRuntime JSRuntime { get; set; }

    [Parameter, EditorRequired]
    public required RenderFragment Content { get; set; }

    private DotNetObjectReference<PageInteractionHandler>? DotNetRef { get; set; }

    private bool _first = false;
    private int _themeHash = -1;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            _first = true;
            await JSRuntime.InvokeVoidAsync("Util.registerKeyDownHandler", GetDotNetReference(), nameof(OnKeyDownAsync));
        }

        if (_first)
        {
            ScopedDataBus.Theme = await JSRuntime.GetThemeAsync();
            var hash = ScopedDataBus.Theme.GetHashCode();
            if (hash != _themeHash)
            {
                _themeHash = hash;
                await InvokeAsync(StateHasChanged)
                    .ConfigureAwait(false);
            }
        }
    }

    protected DotNetObjectReference<PageInteractionHandler> GetDotNetReference()
    {
        DotNetRef ??= DotNetObjectReference.Create(this);

        return DotNetRef;
    }

    public async Task OnClickAsync(MouseEventArgs e)
        => await ScopedDataBus.SendPageClickEventAsync(this, new(e));

    [JSInvokable]
    public async Task OnKeyDownAsync(OnPressEventArgs e)
        => await ScopedDataBus.SendKeyPressEventAsync(this, e);

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                _ = Task.Run(async () =>
                {
                    try
                    {
                        await JSRuntime.InvokeVoidAsync("Util.removeKeyDownHandler", GetDotNetReference());
                    }
                    catch
                    {
                        // If this errors the event handler was removed anyways.
                    }
                });
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}

