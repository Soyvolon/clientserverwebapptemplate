﻿using ClientServerWebAppTemplate.Structures.Events;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClientServerWebAppTemplate.UI.Framework.Handlers;

public partial class AbsoluteMenuPopupHandler : ComponentBase, IDisposable
{
    private bool disposedValue;
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public required NavigationManager NavigationManager { get; set; }

    [Parameter]
    public bool UseSpanMount { get; set; } = true;

    private ConcurrentDictionary<string, DisplayMenuEventArgs> Menus { get; set; } = [];
    private List<string> Order { get; set; } = [];
    private ConcurrentDictionary<string, IDisposable> NavigationInterrupts { get; set; } = [];

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        ScopedDataBus.DisplayMenu += ScopedDataBus_DisplayMenu;
        ScopedDataBus.CloseMenu += ScopedDataBus_CloseMenu;
        ScopedDataBus.ReloadMenu += ScopedDataBus_ReloadMenu;
        ScopedDataBus.KeyPressed += ScopedDataBus_KeyPressed;
    }

    private Task ScopedDataBus_KeyPressed(object sender, OnPressEventArgs args)
    {
        _ = Task.Run(async () =>
        {
            if (args.Code == "Escape")
            {
                var last = Order.LastOrDefault();
                if (last is not null
                    && Menus.TryGetValue(last, out var menu)
                    && menu.AllowEscapeClose)
                {
                    await ScopedDataBus.CloseMenuAsync(this, last);
                }
            }
        });

        return Task.CompletedTask;
    }

    private Task ScopedDataBus_ReloadMenu(object sender)
    {
        _ = Task.Run(async () => await InvokeAsync(StateHasChanged));

        return Task.CompletedTask;
    }

    private Task ScopedDataBus_DisplayMenu(object sender, DisplayMenuEventArgs args)
    {
        _ = Task.Run(async () =>
        {
            Menus[args.Id] = args;
            Order.Add(args.Id);

            await InvokeAsync(StateHasChanged);

            if (args.LockoutNavigation)
            {
                NavigationInterrupts[args.Id] = NavigationManager.RegisterLocationChangingHandler((e) =>
                {
                    e.PreventNavigation();
                    return ValueTask.CompletedTask;
                });
            }
        });

        return Task.CompletedTask;
    }

    private Task ScopedDataBus_CloseMenu(object sender, string id)
    {
        _ = Task.Run(async () =>
        {
            bool removed = Menus.TryRemove(id, out var args);
            _ = Order.Remove(id);

            if (NavigationInterrupts.TryRemove(id, out var interrupt))
                interrupt.Dispose();

            await InvokeAsync(StateHasChanged);

            if (removed)
                await ScopedDataBus.MenuClosedAsync(this, args!);
        });

        return Task.CompletedTask;
    }

#nullable disable
    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                ScopedDataBus.DisplayMenu -= ScopedDataBus_DisplayMenu;
                ScopedDataBus.CloseMenu -= ScopedDataBus_CloseMenu;
                ScopedDataBus.ReloadMenu -= ScopedDataBus_ReloadMenu;
            }

            ScopedDataBus = null;
            Menus = null;
            Order = null;
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
#nullable enable
}
