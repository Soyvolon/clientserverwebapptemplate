﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Handlers;

public class HTMLRenderedHandler : ComponentBase
{
    [Parameter]
    public Func<Task>? OnRender { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender && OnRender is not null)
        {
            await OnRender.Invoke();
        }
    }
}
