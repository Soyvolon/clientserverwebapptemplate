using ClientServerWebAppTemplate.UI.Services.Alert;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Alert;

// https://gitlab.com/Soyvolon/DataCore/-/blob/8cf5568538c97c11e1639163c7c61f122647e92a/ProjectDataCore.Components/Alert/AlertContainer.razor.cs
public partial class AlertContainer : ComponentBase, IDisposable
{
    [Inject]
    public IAlertService AlertService { get; set; }

    private List<AlertModel> _alerts = new();

    protected async override Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();
        _alerts = AlertService.Alerts;
        AlertService.AlertsChanged += AlertsChanged;
    }

    public void AlertsChanged(object? sender, EventArgs e)
    {
        _alerts = AlertService.Alerts;
        InvokeAsync(StateHasChanged);
    }

    public void Dispose()
    {
        AlertService.AlertsChanged -= AlertsChanged;
    }
}
