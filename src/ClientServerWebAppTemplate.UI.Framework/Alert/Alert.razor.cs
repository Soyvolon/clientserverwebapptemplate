using ClientServerWebAppTemplate.Structures.Enums;
using ClientServerWebAppTemplate.UI.Services.Alert;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Alert;

// https://gitlab.com/Soyvolon/DataCore/-/blob/8cf5568538c97c11e1639163c7c61f122647e92a/ProjectDataCore.Components/Alert/Alert.razor.cs
public partial class Alert : ComponentBase
{
    [Parameter]
    public AlertModel? AlertItem { get; set; }

    [Inject]
    public required IAlertService AlertService { get; set; }


    private string _alertColor
    {
        get
        {
            switch (AlertItem.AlertType)
            {
                case AlertType.Success: return "success";
                case AlertType.Warn: return "warn";
                case AlertType.Error: return "error";
                case AlertType.Info: return "info";
                default: return "";
            }
        }
    }

    protected async override Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (AlertItem?.Timer is not null
            && AlertItem.IsRunning)
        {
            AlertItem.Timer.Start();
            AlertItem.Stopwatch.Start();
        }
    }

    private void CloseAlert()
    {
        if (AlertItem is not null)
            AlertService.DeleteAlert(AlertItem);
    }

    private void OnMouseEnter()
    {
        //Console.WriteLine("Mouse Enter");
        if (AlertItem?.Timer is not null
            && AlertItem.IsRunning)
        {
            AlertItem.Stopwatch.Stop();
            AlertItem.Timer.Stop();
        }
    }

    private void OnMouseLeave()
    {
        //Console.WriteLine("Mouse Leave");
        if (AlertItem?.Timer is not null
            && AlertItem.IsRunning)
        {
            AlertItem.Timer.Interval = AlertItem.Stopwatch.ElapsedMilliseconds;
            AlertItem.Timer?.Start();
            AlertItem.Stopwatch.Start();
        }
    }
}