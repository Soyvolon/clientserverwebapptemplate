﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Elements;
public partial class FadeableGoogleIcon : GoogleIconBase
{
    /// <summary>
    /// Flips the function of this icon to be
    /// a click to turn off rather than click
    /// to turn on.
    /// </summary>
    [Parameter]
    public bool Flip { get; set; } = false;

    [Parameter]
    public bool Value { get; set; }

    [Parameter]
    public EventCallback<bool> ValueChanged { get; set; }

    [Parameter]
    public string OuterCss { get; set; } = "";

    private async Task SetValueAsync()
    {
        Value = !Flip;

        await ValueChanged.InvokeAsync(Value);
    }
}
