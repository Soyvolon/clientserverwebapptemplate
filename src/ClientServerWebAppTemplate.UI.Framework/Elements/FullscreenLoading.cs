﻿using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Elements;
public class FullscreenLoading : ComponentBase, IDisposable
{
    public const string PopupId = "fullscreen-loading";
    private bool disposedValue;

    private static RenderFragment Fragment => (builder) =>
    {
        builder.OpenElement(0, "div");
        builder.AddAttribute(1, "class", "absolute z-40 transition-all ease-in-out duration-200 w-full h-full " +
            "bg-slate-200/50 grid grid-cols-1 grid-rows-1 place-items-center");
        builder.OpenElement(2, "div");
        builder.AddAttribute(3, "class", "w-1/6 h-1/6 relative");

        builder.OpenComponent(4, typeof(Loading));
        builder.CloseComponent();

        builder.CloseElement();
        builder.CloseElement();
    };

    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await ScopedDataBus.DisplayMenuAsync(this, new(Fragment, PopupId, true, false));
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                ScopedDataBus.CloseMenuAsync(this, PopupId);
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
