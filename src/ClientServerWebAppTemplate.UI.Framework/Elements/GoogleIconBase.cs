﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Elements;

public abstract class GoogleIconBase : ComponentBase
{
    [Parameter, EditorRequired]
    public required string Icon { get; set; }

    [Parameter]
    public string? Label { get; set; } = null;

    [Parameter]
    public string IconCss { get; set; } = string.Empty;

    [Parameter]
    public string LabelCss { get; set; } = string.Empty;

    [Parameter]
    public Action? OnClick { get; set; }

    [Parameter]
    public Func<Task>? OnClickAsync { get; set; }

    protected async Task OnClickInternalAsync()
    {
        OnClick?.Invoke();

        if (OnClickAsync is not null)
            await OnClickAsync();
    }
}
