using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.Grid;
public partial class LayoutGrid
{
    [Parameter, EditorRequired]
    public required RenderFragment ChildContent { get; set; }
}
