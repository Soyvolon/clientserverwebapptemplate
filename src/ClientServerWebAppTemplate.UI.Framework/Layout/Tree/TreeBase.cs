using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.Tree;
public class TreeBase : ComponentBase
{
    [Parameter]
    public required RenderFragment Element { get; set; }

    [Parameter]
    public required RenderFragment ChildNodes { get; set; }

    [Parameter]
    public bool RenderChild { get; set; } = true;

    [CascadingParameter(Name = "TreeViewIndent")]
    public int Indent { get; set; } = -1;
}
