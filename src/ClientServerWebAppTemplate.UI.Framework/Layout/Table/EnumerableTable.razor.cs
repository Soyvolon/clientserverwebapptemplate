using ClientServerWebAppTemplate.Structures.Layout;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.Table;
public partial class EnumerableTable<T>
{
    /// <summary>
    /// The list of items that are present in the
    /// collection.
    /// </summary>
    [Parameter]
    public IEnumerable<T> Items { get; set; } = [];

    /// <summary>
    /// The table header. This value is in a tr element. The context value
    /// contains the basic CSS styles for each th/td.
    /// </summary>
    [Parameter, EditorRequired]
    public required RenderFragment<string> TableHeader { get; set; }

    /// <summary>
    /// The template for a table row. This value is in a tr element. The context value
    /// contains the current row data, and the basic CSS styles
    /// for each td.
    /// </summary>
    [Parameter, EditorRequired]
    public required RenderFragment<(T row, string style)> TableRow { get; set; }

    /// <summary>
    /// If true, show selection checkboxes on every row of the table.
    /// </summary>
    [Parameter]
    public bool HasSelectionBoxes { get; set; } = false;

    /// <summary>
    /// The selection data for selected rows. Must have
    /// a value if <see cref="HasSelectionBoxes"/> is true.
    /// </summary>
    [Parameter]
    public RowSelectionData? SelectionData { get; set; }
}
