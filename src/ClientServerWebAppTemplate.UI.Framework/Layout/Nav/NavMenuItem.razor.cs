﻿using ClientServerWebAppTemplate.UI.Services.Alert;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.Nav;
public partial class NavMenuItem
{
    [Inject]
    public required IAlertService AlertService { get; set; }

    private Dictionary<string, string>? Urls { get; set; } = null;

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);
    }
}

