﻿using ClientServerWebAppTemplate.Structures.Util.Extensions;
using ClientServerWebAppTemplate.UI.Services.Alert;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Configuration;
using Microsoft.JSInterop;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.Nav;

public partial class NavMenu : ComponentBase
{
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IJSRuntime JSInterop { get; set; }
    [Inject]
    public required IConfiguration Configuration { get; set; }
    [Inject]
    public required NavigationManager NavigationManager { get; set; }

    protected string LoginString
    {
        // TODO: handle redirects.
        get => $"/account/login";
    }

    protected const string LogoutString = "/account/logout";

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await LoadNavbarPagesAsync()
                .ConfigureAwait(false);

            await InvokeAsync(StateHasChanged)
                .ConfigureAwait(false);
        }
    }

    protected async Task LoadNavbarPagesAsync()
    {
        // TODO: Navbar loading.

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    protected void Login()
        => NavigationManager.NavigateTo(LoginString, true);

    protected void Logout()
        => NavigationManager.NavigateTo(LogoutString, true);

    protected async Task SetThemeAsync(string? theme)
    {
        await JSInterop.SetThemeAsync(theme);
        ScopedDataBus.Theme = theme ?? Configuration["DefaultTheme"] ?? "light";

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }
}
