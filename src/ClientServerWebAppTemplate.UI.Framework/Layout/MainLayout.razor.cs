namespace ClientServerWebAppTemplate.UI.Framework.Layout;

public partial class MainLayout
{
    protected List<(string, string)>? Sidebar { get; set; } = null;

    public void SetSidebar(List<(string, string)>? sidebar)
    {
        Sidebar = sidebar;

        _ = InvokeAsync(StateHasChanged);
    }
}