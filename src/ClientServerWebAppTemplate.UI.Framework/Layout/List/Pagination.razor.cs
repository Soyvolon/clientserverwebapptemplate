﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.List;
public partial class Pagination : ComponentBase
{
    [Parameter, EditorRequired]
    public required int Count { get; set; }
    [Parameter, EditorRequired]
    public required int ItemsPerPage { get; set; }

    [Parameter]
    public int Index { get; set; } = 0;
    [Parameter]
    public Action<int>? OnIndexChanged { get; set; }
    [Parameter]
    public Func<int, Task>? OnIndexChangedAsync { get; set; }

    protected int TotalPages { get; set; }
    protected int CurrentPage { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (ItemsPerPage <= 0)
            throw new ArgumentException($"{nameof(ItemsPerPage)} must be a non-zero integer.",
                nameof(ItemsPerPage));
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            TotalPages = (int)Math.Ceiling(Count / (double)ItemsPerPage);
            CurrentPage = Index / ItemsPerPage;
            StateHasChanged();
        }
    }

    protected async Task OnIndexChangedAsyncInternal(int move)
    {
        Index += move;
        CurrentPage = Index / ItemsPerPage;

        OnIndexChanged?.Invoke(Index);

        if (OnIndexChangedAsync is not null)
            await OnIndexChangedAsync.Invoke(Index);

        await InvokeAsync(StateHasChanged);
    }
}
