﻿using ClientServerWebAppTemplate.Structures.Pagination;
using ClientServerWebAppTemplate.Structures.Result;
using ClientServerWebAppTemplate.Structures.Util.Interfaces;
using ClientServerWebAppTemplate.UI.Services.Alert;

using Microsoft.AspNetCore.Components;

using System.Diagnostics.CodeAnalysis;

namespace ClientServerWebAppTemplate.UI.Framework.Layout.List;
public partial class ListRenderer<TValue>
{
    [method: SetsRequiredMembers]
    public class ListRenderContext<T>(T value, int order, int index)
    {
        public required T Value { get; set; } = value;
        public required int Order { get; set; } = order;
        public required int Index { get; set; } = index;
    }

    [Inject]
    public required IAlertService AlertService { get; set; }

    [Parameter, EditorRequired]
    public required RenderFragment<ListRenderContext<TValue>> ChildContent { get; set; }

    [Parameter]
    public IList<TValue> Value { get; set; } = Array.Empty<TValue>();
    [Parameter]
    public EventCallback<IList<TValue>> ValueChanged { get; set; }

    [Parameter]
    public bool Sortable { get; set; } = false;
    //[Parameter]
    //public string Identifier { get; set; } = string.Empty;
    [Parameter]
    public ISortable? SortableParent { get; set; } = null;
    /// <summary>
    /// True if pagination is enabled.
    /// </summary>
    [Parameter]
    public bool Paginate { get; set; } = false;
    [Parameter]
    public int MaxItemsPerPage { get; set; } = 10;
    /// <summary>
    /// The event to call when the pagination is updated.
    /// </summary>
    [Parameter]
    public Action<PaginationRecord>? OnPageChanged { get; set; }
    /// <summary>
    /// Instead of getting values from the items list,
    /// get values from a service method.
    /// </summary>
    [Parameter]
    public Func<PaginationRecord, Task<ActionResult<List<TValue>>>>? OnGetPageAsync { get; set; }
    /// <summary>
    /// Used with <see cref="OnGetPageAsync"/> to get the max
    /// number of elements in the list.
    /// </summary>
    [Parameter]
    public Func<Task<ActionResult<int>>>? OnGetMaxItemsAsync { get; set; }

    /// <summary>
    /// Assigns a reload function for other sources to call
    /// a reload of the data provided by <see cref="OnGetPageAsync"/>.
    /// </summary>
    [Parameter]
    public Action<Func<Task>>? AssignReload { get; set; }

    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }

    private bool UseDynamicGet { get => OnGetMaxItemsAsync is not null && OnGetPageAsync is not null; }
    private int PaginationIndex { get; set; } = 0;
    private int PaginationMaxIndex { get; set; } = 0;

    private int _itemCount = 0;

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        PaginationMaxIndex = PaginationIndex + MaxItemsPerPage;

        if (UseDynamicGet)
        {
            var getRes = await OnGetMaxItemsAsync!();

            if (!getRes.GetResult(out var err, out var count))
            {
                AlertService.CreateErrorAlert(err);
                return;
            }

            _itemCount = count;
        }
        else
        {
            if (_itemCount != Value.Count)
            {
                _itemCount = Value.Count;
            }
        }

        AssignReload?.Invoke(ReloadAsync);
    }

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            await LoadItemsAsync(new(PaginationIndex, PaginationMaxIndex));
        }
    }

    private async Task ReloadAsync()
    {
        await OnParametersSetAsync();

        await LoadItemsAsync(new(PaginationIndex, PaginationMaxIndex));
    }

    private void OnSortUp(int index)
    {
        SortableParent?.RecalculateOrder(index, index - 1);
    }

    private void OnSortDown(int index)
    {
        SortableParent?.RecalculateOrder(index, index + 1);
    }

    private async Task LoadItemsAsync(PaginationRecord record)
    {
        if (!UseDynamicGet)
            return;

        var res = await OnGetPageAsync!(record);
        if (!res.GetResult(out var err, out var items))
        {
            AlertService.CreateErrorAlert(err);
            return;
        }

        Value = items;
        await ValueChanged.InvokeAsync(Value);

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }

    private async Task OnPaginationChangedAsync(int index)
    {
        PaginationIndex = index;
        PaginationMaxIndex = PaginationIndex + MaxItemsPerPage;

        PaginationRecord record = new(PaginationIndex, MaxItemsPerPage);
        OnPageChanged?.Invoke(record);

        await LoadItemsAsync(record);

        await InvokeAsync(StateHasChanged)
            .ConfigureAwait(false);
    }
}
