using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Status;
public partial class Unauthorized
{
    [Inject]
    public required NavigationManager NavigationManager { get; set; }

    protected Task OnLoginClickedAsync()
    {
        var url = NavigationManager.Uri;

        NavigationManager.NavigateTo($"/account/login?redirect={url}");

        return Task.CompletedTask;
    }
}
