﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Menu;
public class PopupMenuBase : ComponentBase, IDisposable
{
    private bool disposedValue;

    [Parameter, EditorRequired]
    public required RenderFragment Popup { get; set; }
    [Parameter, EditorRequired]
    public required RenderFragment Element { get; set; }

    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }

    protected string CssClass
    {
        get
        {
            if (AdditionalAttributes?.TryGetValue("class", out var str) ?? false)
                return (string)str;

            return "";
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {

            }

            AdditionalAttributes = null;
            Popup = null!;
            Element = null!;

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
