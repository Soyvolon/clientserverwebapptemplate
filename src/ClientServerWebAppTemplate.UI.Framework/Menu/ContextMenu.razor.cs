﻿using ClientServerWebAppTemplate.Structures.Events;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace ClientServerWebAppTemplate.UI.Framework.Menu;
public partial class ContextMenu : PopupMenuBase
{
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IScopedDataBus ScopedDataBus { get; set; }
    [Inject]
    public IJSRuntime JSRuntime { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [Parameter]
    public bool CloseWhenOutsideClickOccours { get; set; } = true;
    [Parameter]
    public bool OpenOnLeftClick { get; set; } = false;
    [Parameter, EditorRequired]
    public string Identifier { get; set; } = "";

    private class WH
    {
        public int Width { get; set; }
        public int Height { get; set; }
    }

    private class BoundingBox
    {
        public double Width { get; set; }
        public double Height { get; set; }
        public double Top { get; set; }
        public double Bottom { get; set; }
        public double Left { get; set; }
        public double Right { get; set; }
    }

    private BoundingBox? _boundingBox;
    private bool _left = true;
    private double _xPos = 0;

    private bool _top = true;
    private double _yPos = 0;
    private bool disposedValue;

    private bool _positionReady = false;
    private bool _menuReady = false;

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        ScopedDataBus.MenuClosed += ScopedDataBus_MenuClosed;
    }

    private Task ScopedDataBus_MenuClosed(object sender, DisplayMenuEventArgs args)
    {
        if (args.Id != Identifier)
            return Task.CompletedTask;

        _ = Task.Run(async () =>
        {
            await AbortMenuAsync();
        });

        return Task.CompletedTask;
    }

    private async Task OnMenuLoaded()
    {
        if (!_menuReady)
        {
            _menuReady = true;

            ScopedDataBus.PageClicked += ScopedDataBus_PageClicked;

            await ScopedDataBus.RequestMenuReload(this);
        }
    }

    private async Task OnMenuReady()
    {
        if (_menuReady)
        {
            if (CloseWhenOutsideClickOccours)
            {
                _boundingBox = await JSRuntime.InvokeAsync<BoundingBox>("Util.getBoundingBox", Identifier);
            }
        }
    }

    private async Task OnContextMenu(MouseEventArgs args)
    {
        _xPos = args.PageX;
        _yPos = args.PageY;

        var wh = await JSRuntime.InvokeAsync<WH>("Util.getWindowDimensions");

        AdjustPosition(wh);

        _positionReady = true;

        StateHasChanged();
    }

    private async Task OnClick(MouseEventArgs args)
    {
        if (OpenOnLeftClick)
            await OnContextMenu(args);
    }

    private void AdjustPosition(WH screen)
    {
        if (_xPos > screen.Width / 2)
        {
            _xPos = screen.Width - _xPos;
            _left = false;
        }

        if (_yPos > screen.Height / 2)
        {
            _yPos = screen.Height - _yPos;
            _top = false;
        }
    }

    private Task ScopedDataBus_PageClicked(object sender, PageClickedEventArgs args)
    {
        _ = Task.Run(async () =>
        {
            if (CloseWhenOutsideClickOccours)
            {
                if (_boundingBox is not null
                    && args is not null)
                {
                    if (args.XPos < _boundingBox.Left
                        || args.XPos > _boundingBox.Right
                        || args.YPos < _boundingBox.Top
                        || args.YPos > _boundingBox.Bottom)
                    {
                        await AbortMenuAsync();
                    }
                }
                else
                {
                    await AbortMenuAsync();
                }
            }
        });

        return Task.CompletedTask;
    }

    private async Task AbortMenuAsync()
    {
        _positionReady = false;
        _menuReady = false;

        ScopedDataBus.PageClicked -= ScopedDataBus_PageClicked;

        await InvokeAsync(StateHasChanged);
    }

    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);

        if (!disposedValue)
        {
            if (disposing)
            {
                ScopedDataBus.PageClicked -= ScopedDataBus_PageClicked;
                ScopedDataBus.MenuClosed -= ScopedDataBus_MenuClosed;
            }

            _boundingBox = null;

            disposedValue = true;
        }
    }
}
