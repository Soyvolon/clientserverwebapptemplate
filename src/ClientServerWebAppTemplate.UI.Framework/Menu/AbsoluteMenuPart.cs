﻿using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Menu;

public partial class AbsoluteMenuPart : ComponentBase, IDisposable
{
    private bool disposedValue;
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IScopedDataBus ScopedDataBus { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

#pragma warning disable CS8618 // Editor required is never null.
    [Parameter, EditorRequired]
    public RenderFragment Display { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    [Parameter]
    public bool OpenMenu { get; set; } = false;
    [Parameter]
    public bool LockoutNavigationOnOpen { get; set; } = false;
    [Parameter]
    public bool EnableEscapeClose { get; set; } = true;
    private bool Opened { get; set; } = false;

    [CascadingParameter(Name = "MenuId")]
    public string Id { get; set; } = "";

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (OpenMenu && !Opened)
        {
            Opened = true;
            await ScopedDataBus.DisplayMenuAsync(this, new(Display, Id, LockoutNavigationOnOpen, EnableEscapeClose));
        }
        else if (!OpenMenu && Opened)
        {
            Opened = false;
            await ScopedDataBus.CloseMenuAsync(this, Id);
        }
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {
                _ = Task.Run(async () => await ScopedDataBus.CloseMenuAsync(this, Id));
            }

            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
