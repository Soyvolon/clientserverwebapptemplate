﻿using ClientServerWebAppTemplate.Structures.Events;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace ClientServerWebAppTemplate.UI.Framework.Menu;

public partial class ModalMenu : PopupMenuBase
{
#pragma warning disable CS8618 // Injections are never null.
    [Inject]
    public IScopedDataBus ScopedDataBus { get; set; }
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    [Parameter, EditorRequired]
    public string Identifier { get; set; } = "";
    [Parameter]
    public bool DisplayCloseButton { get; set; } = true;
    [Parameter]
    public string WidthClass { get; set; } = "w-10/12";
    [Parameter]
    public string HeightClass { get; set; } = "h-5/6";
    [Parameter]
    public string BackgroundClass { get; set; } = "bg-slate-200/50";
    [Parameter]
    public Func<Task>? OnMenuReadyAsync { get; set; }

    private bool _positionReady = false;
    private bool _menuReady = false;

    protected override async Task OnInitializedAsync()
    {
        await base.OnInitializedAsync();

        ScopedDataBus.MenuClosed += ScopedDataBus_MenuClosed;
    }

    private Task OnClick(MouseEventArgs args)
    {
        _positionReady = true;

        StateHasChanged();

        return Task.CompletedTask;
    }

    private async Task OnMenuLoaded()
    {
        if (!_menuReady)
        {
            _menuReady = true;

            await ScopedDataBus.RequestMenuReload(this);
        }
    }

    private async Task OnMenuReady()
    {
        if (_menuReady)
        {
            if (OnMenuReadyAsync is not null)
                await OnMenuReadyAsync.Invoke();
        }
    }

    private Task ScopedDataBus_MenuClosed(object sender, DisplayMenuEventArgs args)
    {
        if (args.Id != Identifier)
            return Task.CompletedTask;

        _ = Task.Run(AbortMenuAsync);

        return Task.CompletedTask;
    }

    private async Task AbortMenuAsync()
    {
        _positionReady = false;
        _menuReady = false;

        await InvokeAsync(StateHasChanged);
    }

    private bool disposedValue = false;
    protected override void Dispose(bool disposing)
    {
        base.Dispose(disposing);

        if (!disposedValue)
        {
            if (disposing)
            {
                ScopedDataBus.MenuClosed -= ScopedDataBus_MenuClosed;
            }

            disposedValue = true;
        }
    }
}
