﻿using ClientServerWebAppTemplate.UI.Services.Alert;
using ClientServerWebAppTemplate.UI.Services.Bus.Scoped;

using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Buttons;

public enum ButtonType
{
    Success,
    Info,
    Warning,
    Danger
}

public partial class FormButton
{
    const string SuccessCss = "rounded-lg p-2 border-green-200 hover:border-green-600 border hover:shadow-md bg-green-200 w-full active:bg-green-300";
    const string InfoCss = "rounded-lg p-2 border-blue-200 hover:border-blue-600 border hover:shadow-md bg-blue-200 w-full active:bg-blue-300";
    const string WarningCss = "rounded-lg p-2 border-orange-200 hover:border-orange-600 border hover:shadow-md bg-orange-200 w-full active:bg-orange-300";
    const string DangerCss = "rounded-lg p-2 border-red-200 hover:border-red-600 border hover:shadow-md bg-red-200 w-full active:bg-red-300";

    const string ConfirmSuccessCss = "rounded-lg border-green-400 hover:border-green-800 border bg-green-400 px-4 py-1";
    const string ConfirmInfoCss = "rounded-lg border-blue-400 hover:border-blue-800 border bg-blue-400 px-4 py-1";
    const string ConfirmWarningCss = "rounded-lg border-orange-400 hover:border-orange-800 border bg-orange-400 px-4 py-1";
    const string ConfirmDangerCss = "rounded-lg border-red-400 hover:border-red-800 border bg-red-400 px-4 py-1";

    const string PopupId = "button-confirm-prompt";

    [Inject]
    public required IAlertService AlertService { get; set; }
    [Inject]
    public required IScopedDataBus ScopedDataBus { get; set; }

    [Parameter]
    public RenderFragment? ChildContent { get; set; }

    [Parameter]
    public ButtonType ButtonType { get; set; } = ButtonType.Info;

    [Parameter]
    public bool Disabled { get; set; }
    [Parameter]
    public string? ConfirmationMessage { get; set; } = null;
    /// <summary>
    /// If a value is set, this value must be type in by the user
    /// to confirm a button click. The <see cref="ConfirmationMessage"/>
    /// value is used on the confirm button for the prompt.
    /// </summary>
    [Parameter]
    public string? ConfirmationTypePrompt { get; set; } = null;

    [Parameter]
    public Action? OnClick { get; set; }
    [Parameter]
    public Func<Task>? OnClickAsync { get; set; }

    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }

    private string _typePromptValue = "";

    protected string CssClass
    {
        get
        {
            if (AdditionalAttributes?.TryGetValue("class", out var str) ?? false)
                return (string)str;

            return "";
        }
    }

    protected string ButtonCss
    {
        get => ButtonType switch
        {
            ButtonType.Success => SuccessCss,
            ButtonType.Info => InfoCss,
            ButtonType.Warning => WarningCss,
            ButtonType.Danger => DangerCss,
            _ => ""
        };
    }

    protected string ConfirmButtonCss
    {
        get => ButtonType switch
        {
            ButtonType.Success => ConfirmSuccessCss,
            ButtonType.Info => ConfirmInfoCss,
            ButtonType.Warning => ConfirmWarningCss,
            ButtonType.Danger => ConfirmDangerCss,
            _ => ""
        };
    }

    protected virtual async Task OnClickInternalAsync(MouseEventArgs args)
    {
        if (Disabled)
            return;

        OnClick?.Invoke();

        if (OnClickAsync is not null)
            await OnClickAsync.Invoke();
    }

    protected virtual async Task OnClickPromptInternalAsync(MouseEventArgs args)
    {
        if (_typePromptValue != ConfirmationTypePrompt)
        {
            AlertService.CreateWarnAlert("The text does not match.");
            return;
        }

        _typePromptValue = "";

        await OnClickInternalAsync(args);

        await ScopedDataBus.CloseMenuAsync(this, PopupId);
    }
}
