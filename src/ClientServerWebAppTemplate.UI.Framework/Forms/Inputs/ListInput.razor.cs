﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;

public partial class ListInput<TValue> : CustomInputBase<TValue>
{
    [Parameter]
    public List<TValue> Items { get; set; } = [];

    [Parameter]
    public Func<TValue, string> LabelFunction { get; set; } = (e) => $"{e}";

    [Parameter]
    public string? Label { get; set; } = "";

    private string _labelValue = "";

    private async Task OnLabelValueChangedAsync(string label)
    {
        _labelValue = label;

        var item = Items
            .Where(e => LabelFunction(e).Equals(label))
            .First();

        await OnValueChangedInternalAsync(item);
    }
}
