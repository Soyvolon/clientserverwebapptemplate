﻿using Microsoft.AspNetCore.Components;

using System.Collections;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class ListModifier<T> : CustomInputBase<T>
    where T : IList
{
    private string _input = "";

    [Parameter]
    public string Label { get; set; } = "";

    protected async Task OnAddAsync()
    {
        Value.Add(_input);
        _input = "";
        StateHasChanged();

        await OnValueChangedInternalAsync(Value);
    }

    protected async Task OnRemoveAsync(int index)
    {
        Value.RemoveAt(index);
        StateHasChanged();

        await OnValueChangedInternalAsync(Value);
    }
}
