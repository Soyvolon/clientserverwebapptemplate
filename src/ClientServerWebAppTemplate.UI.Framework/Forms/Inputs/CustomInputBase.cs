﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class CustomInputBase<T> : ComponentBase, IDisposable
{
    private bool disposedValue;

    [CascadingParameter(Name = "DisableEdits")]
    public bool DisableEdits { get; set; } = false;

    [Parameter]
    public EventCallback<T> ValueChanged { get; set; }

    [Parameter]
    public T Value { get; set; } = default!;

    [Parameter(CaptureUnmatchedValues = true)]
    public IReadOnlyDictionary<string, object>? AdditionalAttributes { get; set; }

    protected string CssClass
    {
        get
        {
            if (AdditionalAttributes?.TryGetValue("class", out var str) ?? false)
                return (string)str;

            return "";
        }
    }

    protected string GetCssClass(string moreClasses = "")
        => CssClass + ' ' + moreClasses;

    protected virtual async Task OnValueChangedInternalAsync(T value)
    {
        Value = value;

        if (ValueChanged.HasDelegate)
            await ValueChanged.InvokeAsync(value);

        StateHasChanged();
    }

    protected virtual void Dispose(bool disposing)
    {
        if (!disposedValue)
        {
            if (disposing)
            {

            }

            AdditionalAttributes = null;
            disposedValue = true;
        }
    }

    public void Dispose()
    {
        // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
        Dispose(disposing: true);
        GC.SuppressFinalize(this);
    }
}
