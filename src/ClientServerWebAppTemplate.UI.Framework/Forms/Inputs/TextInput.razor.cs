﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class TextInput : CustomInputBase<string>
{
    [Parameter]
    public string? Label { get; set; } = "";

    protected Dictionary<string, object?> GetAdditionalAttributes()
    {
        Dictionary<string, object?> attrs = new()
        {
            { "class", "input-control peer" },
            { "placeholder", "Option Name" },
            { "disabled", DisableEdits }
        };

        if (AdditionalAttributes is not null)
            foreach (var pair in AdditionalAttributes)
                if (!attrs.TryAdd(pair.Key, pair.Value))
                    if (attrs[pair.Key] is string atr && pair.Value is string p)
                        attrs[pair.Key] = atr + " " + p;

        return attrs;
    }
}
