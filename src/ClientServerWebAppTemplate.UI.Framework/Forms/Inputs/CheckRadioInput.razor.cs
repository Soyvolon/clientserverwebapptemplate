﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class CheckRadioInput : CustomInputBase<bool>
{
    [Parameter]
    public bool Radio { get; set; } = false;

    [Parameter]
    public RenderFragment? ChildContent { get; set; }

    public string Id { get; set; } = Guid.NewGuid().ToString();
}
