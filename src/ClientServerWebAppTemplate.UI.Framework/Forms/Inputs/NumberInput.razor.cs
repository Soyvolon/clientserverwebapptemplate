﻿using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class NumberInput<T> : CustomInputBase<T>
{
    private enum NumType
    {
        Int, Long, Short, Float, Double, Decimal
    }

    private NumType ValueType { get; set; } = NumType.Int;

    [Parameter]
    public string? Label { get; set; } = "";

    protected override async Task OnAfterRenderAsync(bool firstRender)
    {
        await base.OnAfterRenderAsync(firstRender);

        if (firstRender)
        {
            switch (Value)
            {
                case int:
                    ValueType = NumType.Int;
                    break;
                case long:
                    ValueType = NumType.Long;
                    break;
                case short:
                    ValueType = NumType.Short;
                    break;
                case float:
                    ValueType = NumType.Float;
                    break;
                case double:
                    ValueType = NumType.Double;
                    break;
                case decimal:
                    ValueType = NumType.Decimal;
                    break;
            }
        }
    }
}
