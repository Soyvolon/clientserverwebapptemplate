﻿using ClientServerWebAppTemplate.Structures.Util.Extensions;

using Microsoft.AspNetCore.Components;

namespace ClientServerWebAppTemplate.UI.Framework.Forms.Inputs;
public partial class EnumListInput<TEnum> : CustomInputBase<TEnum>
    where TEnum : Enum
{
    [Parameter]
    public string? Label { get; set; } = "";

    private List<string> Items { get; set; } = [];
    private string? ListValue { get; set; } = "";

    protected override void OnParametersSet()
    {
        base.OnParametersSet();

        Items.Clear();
        foreach (TEnum val in Enum.GetValues(typeof(TEnum)))
        {
            Items.Add(val.AsFull());
        }

        ListValue = Value?.AsFull();
    }

    protected async Task OnListInputChangedAsync(string val)
    {
        var cur = FindMatch(val);

        await OnValueChangedInternalAsync(cur);
    }

    private static TEnum FindMatch(string test)
    {
        foreach (TEnum val in Enum.GetValues(typeof(TEnum)))
        {
            if (val.AsFull() == test)
                return val;
        }

        return default!;
    }
}
