using ClientServerWebAppTemplate.Structures.Http;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

using System.Collections.Concurrent;
using System.Reflection;

namespace ClientServerWebAppTemplate.Server;

public class Startup(IConfiguration configuration)
{
    private static ConcurrentDictionary<string, SecurityKey> BearerKeys { get; } = new();

    public IConfiguration Configuration { get; } = configuration;

    private readonly string[] xmlFilenames = [
        $"{Assembly.GetExecutingAssembly().GetName().Name}.xml",
        $"{Assembly.GetAssembly(typeof(EndpointData))?.GetName().Name}.xml"
    ];

    public void ConfigureServices(IServiceCollection services)
    {
#if DEBUG
        IdentityModelEventSource.ShowPII = true;
#endif

        // Add services to the container.
        services.AddControllers();

        // For future reference: https://stackoverflow.com/a/73683912/11682098
        services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(options =>
            {
                var keyCfg = Configuration.GetRequiredSection("Config:keys");
                var keys = keyCfg.Get<JsonWebKey[]>();

                foreach (var key in keys)
                {
                    if (!BearerKeys.TryAdd(key.KeyId, key))
                        throw new Exception("Failed to add new public key. Make sure key ids are unique.");
                }

                options.TokenValidationParameters = new()
                {
                    IssuerSigningKeys = BearerKeys.Values,

                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,

                    ValidIssuer = Configuration["Config:Issuer"] ?? "",
                    ValidAudience = Configuration["Config:Audience"] ?? "",

                    IssuerSigningKeyResolver = (token, sec, kid, param) =>
                    {
                        if (BearerKeys.TryGetValue(kid, out var key))
                            return [key];

                        return [];
                    }
                };

                options.Validate();
            });

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v0.1.0",
                Title = "Web Server API",
                Description = "An API for the web server part of the app."
            });

            foreach (var xml in xmlFilenames)
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xml));
        });
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        // Configure the HTTP request pipeline.
        if (env.IsDevelopment() || env.IsProduction())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseForwardedHeaders(new ForwardedHeadersOptions()
        {
            ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
        });

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
