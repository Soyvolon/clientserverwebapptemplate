using ClientServerWebAppTemplate.Structures.Http.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ClientServerWebAppTemplate.Server.Controllers.v1.Account;

/// <summary>
/// Provides endpoints for managing general user data.
/// </summary>
[Route($"{v1.VersionHelpers.BASE_ROUTE}/user")]
[Authorize]
public class UserController : Controller
{
    /// <summary>
    /// Gets all service subscriptions for a user.
    /// </summary>
    /// <returns>A JSON array of service subscriptions.</returns>
    [HttpGet("subscriptions")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string[]))]
    [Produces("application/json")]
    [APIReturnType(typeof(string[]))]
    public Task<ActionResult> GetUserSubscriptions()
    {
        return Task.FromResult<ActionResult>(Json(new string[] { "a", "b", "c" }));
    }
}
