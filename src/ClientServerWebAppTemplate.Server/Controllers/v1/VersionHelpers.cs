namespace ClientServerWebAppTemplate.Server.Controllers.v1;

internal static class VersionHelpers
{
    public const string VERSION = "v1";
    public const string BASE_ROUTE = "/api/" + VERSION;
}
