using ClientServerWebAppTemplate.Structures.Http;
using ClientServerWebAppTemplate.Structures.Http.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.ModelBinding;

using System.Reflection;

namespace ClientServerWebAppTemplate.Server.Controllers;
// Controller from https://stackoverflow.com/a/66086633/11682098

/// <summary>
/// Provides information about the API.
/// </summary>
/// <remarks>
/// Builds services for the controller.
/// </remarks>
/// <param name="endpointSources">A required service.</param>
[Route("/-/info")]
public class InfoController(
        IEnumerable<EndpointDataSource> endpointSources
    ) : Controller
{
    /// <summary>
    /// Gets all endpoints in this API.
    /// </summary>
    /// <returns>A JSON representation of all endpoints.</returns>
    [HttpGet("endpoints")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(EndpointData[]))]
    [Produces("application/json")]
    [APIReturnType(typeof(EndpointData[]))]
    public Task<ActionResult> ListAllEndpoints()
    {
        var output = GetEndpointDataList();

        return Task.FromResult<ActionResult>(Json(output));
    }

    /// <summary>
    /// Get the endpoint data.
    /// </summary>
    /// <returns>A list of endpoint data.</returns>
    [NonAction]
    public List<EndpointData> GetEndpointDataList()
    {
        var endpoints = endpointSources
            .SelectMany(es => es.Endpoints)
            .OfType<RouteEndpoint>();
        var output = endpoints.Select(
            e =>
            {
                var controller = e.Metadata
                    .OfType<ControllerActionDescriptor>()
                    .FirstOrDefault();
                var action = controller != null
                    ? $"{controller.ControllerName}.{controller.ActionName}"
                    : null;
                var controllerMethod = controller != null
                    ? $"{controller.ControllerTypeInfo.FullName}:{controller.MethodInfo.Name}"
                    : null;

                var apiReturnAttribute = controller?.MethodInfo
                    .GetCustomAttributes<APIReturnTypeAttribute>()
                    .FirstOrDefault();

                var parameters = controller?.MethodInfo.GetParameters();

                var paramAttrs = parameters?.Select(e =>
                {
                    var attrs = e.GetCustomAttributes()
                        .Where(e => e is IBindingSourceMetadata)
                        .ToArray();

                    return attrs;
                }).ToArray();

                var authorize = e.Metadata.GetMetadata<AuthorizeAttribute>();

                return new EndpointData()
                {
                    Method = e.Metadata.OfType<HttpMethodMetadata>().FirstOrDefault()?.HttpMethods?[0],
                    Route = $"/{e.RoutePattern.RawText?.TrimStart('/')}",
                    Action = action,
                    ControllerMethod = controllerMethod,
                    ReturnType = string.Format("{0}.{1}", apiReturnAttribute?.Type.Namespace, apiReturnAttribute?.Type.Name),
                    ParameterNames = parameters?.Select(p => p.Name ?? "").ToArray(),
                    ParameterTypes = parameters?.Select(p => string.Format("{0}.{1}", p?.ParameterType.Namespace, p?.ParameterType.Name)).ToArray(),
                    ParameterAttributes = paramAttrs,
                    AuthorizationRequired = authorize is not null
                };
            }
        );

        return output.ToList();
    }
}
