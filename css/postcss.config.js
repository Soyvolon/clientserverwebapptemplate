const plugins = [
    // 'C:\\Users\\AriaBounds\\source\\repos\\postcss-import-rewrite'
    require('postcss-import-rewrite')({
        envvars: [
            'DOTNET_PROJECT_DIR',
            'DOTNET_CONFIG_NAME',
            'DOTNET_TARGET_FRAMEWORK',
        ],
        rewrites: [
            {
                template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
                match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
            },
            {
                template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
                match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
            }
        ],
        deep: true,
        log: true,
        logFile: 'css/logs/debug.log'
    }),
    require('postcss-import'),
    require('tailwindcss')({
        config: 'css/tailwind.config.js'
    }),
    require('autoprefixer')
];

const yargs = require('yargs/yargs')
const { hideBin } = require('yargs/helpers')
const argv = yargs(hideBin(process.argv)).parse()

async function run() {
    const inFile = argv.in;
    const outFile = argv.out;

    const postcss = require('postcss');
    const fs = require('fs');

    const css = await fs.promises.readFile(inFile);

    let res = await postcss(plugins)
        .process(css, { from: inFile, to: outFile });

    for (let i = 0; i < res.messages.length; i++) {
        const message = res.messages[i];
        console.log(message);
    }

    await fs.promises.writeFile(outFile, res.css);
}

run();
