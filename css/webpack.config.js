const path = require('path');
const root = path.resolve(__dirname, '../src/ClientServerWebAppTemplate.UI/wwwroot');
const dist = path.resolve(root, 'dist');

module.exports = () => {
    return [
        {
            name: 'Site Utils',
            entry: path.resolve(root, './site.js'),
            output: {
                path: dist,
                filename: 'site.bundle.js',
            }
        }
    ];
};

module.exports.parallelism = 1;