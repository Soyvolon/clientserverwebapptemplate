module.exports = {
    plugins: [
        require('C:\\Users\\AriaBounds\\source\\repos\\postcss-import-rewrite')({
            envvars: [
                'DOTNET_PROJECT_DIR',
                'DOTNET_CONFIG_NAME',
                'DOTNET_TARGET_FRAMEWORK',
            ],
            rewrites: [
                {
                    template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\projectbundle\\{4}",
                    match: /^'_content\/([\w.]*)\/([\w.]*bundle\.scp\.css'?)$/g
                },
                {
                    template: "'{0}\\{3}\\obj\\{1}\\{2}\\scopedcss\\bundle\\{4}",
                    match: /^'_content\/([\w.]*)\/([\w.]*styles\.css'?)$/g
                }
            ],
            deep: true,
            log: true,
            logFile: 'css/logs/debug.log'
        }),
        require('postcss-import'),
        require('tailwindcss')({
            config: 'css/tailwind.config.js'
        }),
        require('autoprefixer')
    ]
}

async function run() {
    const postcss = require('postcss');
    const fs = require('fs');

    process.env['DOTNET_CONFIG_NAME'] = 'Debug';
    process.env['DOTNET_TARGET_FRAMEWORK'] = 'net8.0';
    process.env['DOTNET_PROJECT_DIR'] = 'C:\\Users\\AriaBounds\\source\\repos\\ClientServerWebAppTemplate\\src';

    const folder = `src\\ClientServerWebAppTemplate.UI\\wwwroot\\css`
    const file = `${folder}\\Debug.css`;
    const outFile = `${folder}\\app.min.css`;
    const css = await fs.promises.readFile(file);

    let res = await postcss(module.exports.plugins)
        .process(css, { from: file, to: outFile });

    for (let i = 0; i < res.messages.length; i++) {
        const message = res.messages[i];
        console.log(message);
    }
    
    await fs.promises.writeFile(outFile, res.css);
}

run().catch(e => console.log("error: " + e)).finally(() => console.log("Done."));
